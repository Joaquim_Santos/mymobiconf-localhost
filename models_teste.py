# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Apresentacao(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=50)  # Field name made lowercase.
    palavras_chave = models.CharField(db_column='Palavras_chave', max_length=100)  # Field name made lowercase.
    apresentadores = models.CharField(db_column='Apresentadores', max_length=150)  # Field name made lowercase.
    id_atividade = models.ForeignKey('Atividade', models.DO_NOTHING, db_column='ID_ATIVIDADE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'apresentacao'


class Atividade(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=200)  # Field name made lowercase.
    data = models.DateField(db_column='Data')  # Field name made lowercase.
    hora_inicio = models.TimeField(db_column='Hora_inicio')  # Field name made lowercase.
    hora_fim = models.TimeField(db_column='Hora_fim')  # Field name made lowercase.
    descricao = models.CharField(db_column='Descricao', max_length=2500, blank=True, null=True)  # Field name made lowercase.
    tema = models.CharField(db_column='Tema', max_length=50, blank=True, null=True)  # Field name made lowercase.
    responsavel = models.CharField(db_column='Responsavel', max_length=2000)  # Field name made lowercase.
    id_evento = models.ForeignKey('Evento', models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    id_local = models.ForeignKey('Local', models.DO_NOTHING, db_column='ID_LOCAL', blank=True, null=True)  # Field name made lowercase.
    id_tipo = models.ForeignKey('TipoAtividade', models.DO_NOTHING, db_column='ID_TIPO', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'atividade'


class AtividadeSocial(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome_social = models.CharField(db_column='Nome_social', max_length=50)  # Field name made lowercase.
    data = models.CharField(db_column='Data', max_length=20, blank=True, null=True)  # Field name made lowercase.
    hora = models.TimeField(db_column='Hora', blank=True, null=True)  # Field name made lowercase.
    descricao = models.CharField(db_column='Descricao', max_length=1000)  # Field name made lowercase.
    id_organizador = models.IntegerField(db_column='ID_ORGANIZADOR', blank=True, null=True)  # Field name made lowercase.
    id_evento = models.ForeignKey('Evento', models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    foto_social = models.TextField(db_column='Foto_social', blank=True, null=True)  # Field name made lowercase.
    link_social = models.CharField(db_column='Link_social', max_length=100)  # Field name made lowercase.
    id_facebook = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'atividade_social'


class AtividadeTipoInscricao(models.Model):
    id_atividade = models.ForeignKey(Atividade, models.DO_NOTHING, db_column='ID_ATIVIDADE', primary_key=True)  # Field name made lowercase.
    id_tipo_inscricao = models.ForeignKey('TipoInscricao', models.DO_NOTHING, db_column='ID_TIPO_INSCRICAO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'atividade_tipo_inscricao'


class CheckinSocial(models.Model):
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='ID_PARTICIPANTE', primary_key=True)  # Field name made lowercase.
    id_atividade_social = models.ForeignKey(AtividadeSocial, models.DO_NOTHING, db_column='ID_ATIVIDADE_SOCIAL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'checkin_social'


class ConfirmacaoSocial(models.Model):
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='ID_PARTICIPANTE', primary_key=True)  # Field name made lowercase.
    id_atividade_social = models.ForeignKey(AtividadeSocial, models.DO_NOTHING, db_column='ID_ATIVIDADE_SOCIAL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'confirmacao_social'


class Evento(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sigla = models.CharField(db_column='Sigla', max_length=20)  # Field name made lowercase.
    nome = models.CharField(max_length=100)
    informacoes_gerais = models.CharField(max_length=5000)
    data_inicio = models.DateField(db_column='Data_inicio')  # Field name made lowercase.
    data_fim = models.DateField(db_column='Data_fim')  # Field name made lowercase.
    urllogo = models.CharField(db_column='urlLogo', max_length=400, blank=True, null=True)  # Field name made lowercase.
    local_principal = models.CharField(max_length=140)
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'evento'


class EventoTipoInscricao(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    id_tipo_inscricao = models.ForeignKey('TipoInscricao', models.DO_NOTHING, db_column='ID_TIPO_INSCRICAO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'evento_tipo_inscricao'


class EventoUsuario(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_usuario = models.IntegerField(db_column='ID_USUARIO')  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'evento_usuario'


class Feedback(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='ID_PARTICIPANTE')  # Field name made lowercase.
    opiniao = models.CharField(max_length=2000)

    class Meta:
        managed = False
        db_table = 'feedback'


class Foto(models.Model):
    foto_id = models.AutoField(db_column='FOTO_ID', primary_key=True)  # Field name made lowercase.
    foto_tipo = models.CharField(db_column='FOTO_TIPO', max_length=25)  # Field name made lowercase.
    foto = models.TextField(db_column='FOTO')  # Field name made lowercase.
    foto_tamanho = models.CharField(db_column='FOTO_TAMANHO', max_length=25)  # Field name made lowercase.
    foto_categoria = models.CharField(db_column='FOTO_CATEGORIA', max_length=25)  # Field name made lowercase.
    foto_nome = models.CharField(db_column='FOTO_NOME', max_length=25)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'foto'





class LocaisEvento(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    id_local = models.ForeignKey('Local', models.DO_NOTHING, db_column='ID_LOCAL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'locais_evento'


class Local(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=50)  # Field name made lowercase.
    descricao = models.CharField(db_column='Descricao', max_length=140, blank=True, null=True)  # Field name made lowercase.
    logradouro = models.CharField(db_column='Logradouro', max_length=100, blank=True, null=True)  # Field name made lowercase.
    cep = models.CharField(db_column='CEP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    bairro = models.CharField(db_column='Bairro', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numero = models.IntegerField(db_column='Numero', blank=True, null=True)  # Field name made lowercase.
    cidade = models.CharField(db_column='Cidade', max_length=50, blank=True, null=True)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pais = models.CharField(db_column='Pais', max_length=50, blank=True, null=True)  # Field name made lowercase.
    coordx = models.FloatField(db_column='Coordx', blank=True, null=True)  # Field name made lowercase.
    coordy = models.FloatField(db_column='Coordy', blank=True, null=True)  # Field name made lowercase.
    complemento = models.CharField(db_column='Complemento', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'local'


class NotaAtividadeParticipante(models.Model):
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='id_evento', primary_key=True)
    id_atividade = models.ForeignKey(Atividade, models.DO_NOTHING, db_column='id_atividade')
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='id_participante')
    nota = models.IntegerField()
    data = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'nota_atividade_participante'
        unique_together = (('id_evento', 'id_atividade', 'id_participante'),)


class NotaConfortoSonoroAtividade(models.Model):
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='id_evento', primary_key=True)
    id_atividade = models.ForeignKey(Atividade, models.DO_NOTHING, db_column='id_atividade')
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='id_participante')
    nota = models.IntegerField()
    data = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'nota_conforto_sonoro_atividade'
        unique_together = (('id_evento', 'id_atividade', 'id_participante'),)


class NotaConfortoTermicoAtividade(models.Model):
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='id_evento', primary_key=True)
    id_atividade = models.ForeignKey(Atividade, models.DO_NOTHING, db_column='id_atividade')
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='id_participante')
    nota = models.IntegerField()
    data = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'nota_conforto_termico_atividade'
        unique_together = (('id_evento', 'id_atividade', 'id_participante'),)


class Noticia(models.Model):
    id_noticia = models.AutoField(db_column='ID_NOTICIA', primary_key=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    hora = models.DateTimeField()
    titulo = models.CharField(max_length=100)
    corpo = models.CharField(max_length=5000)

    class Meta:
        managed = False
        db_table = 'noticia'


class Opiniao(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_participante = models.ForeignKey('Participante', models.DO_NOTHING, db_column='ID_PARTICIPANTE', blank=True, null=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    opiniao = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'opiniao'


class Participante(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=50)  # Field name made lowercase.
    senha = models.CharField(max_length=30, blank=True, null=True)
    foto = models.ForeignKey(Foto, models.DO_NOTHING, db_column='FOTO_ID', blank=True, null=True)  # Field name made lowercase.
    instituicao = models.CharField(db_column='INSTITUICAO', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=50)  # Field name made lowercase.
    id_tipo_inscricao = models.ForeignKey('TipoInscricao', models.DO_NOTHING, db_column='ID_TIPO_INSCRICAO', blank=True, null=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'participante'


class Patrocinador(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=500)  # Field name made lowercase.
    url = models.CharField(max_length=500)
    urlimagem = models.CharField(db_column='urlImagem', max_length=500)  # Field name made lowercase.
    prioridade = models.IntegerField()
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'patrocinador'


class Pergunta(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    pergunta = models.CharField(max_length=250)
    id_questionario = models.ForeignKey('Questionario', models.DO_NOTHING, db_column='ID_QUESTIONARIO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pergunta'


class Questionario(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    id_evento = models.ForeignKey(Evento, models.DO_NOTHING, db_column='ID_EVENTO')  # Field name made lowercase.
    id_atividade = models.ForeignKey(Atividade, models.DO_NOTHING, db_column='ID_ATIVIDADE', blank=True, null=True)  # Field name made lowercase.
    nome = models.CharField(max_length=50)
    disponivel = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'questionario'


class Resposta(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    resposta = models.CharField(max_length=150)
    id_pergunta = models.ForeignKey(Pergunta, models.DO_NOTHING, db_column='ID_PERGUNTA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'resposta'


class RespostaParticipante(models.Model):
    id_pergunta = models.ForeignKey(Pergunta, models.DO_NOTHING, db_column='ID_PERGUNTA', primary_key=True)  # Field name made lowercase.
    id_resposta = models.ForeignKey(Resposta, models.DO_NOTHING, db_column='ID_RESPOSTA')  # Field name made lowercase.
    id_participante = models.ForeignKey(Participante, models.DO_NOTHING, db_column='ID_PARTICIPANTE')  # Field name made lowercase.
    hora_resposta = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'resposta_participante'
        unique_together = (('id_pergunta', 'id_participante'),)


class TipoAtividade(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tipo_atividade'


class TipoInscricao(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='NOME', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tipo_inscricao'


class Usuario(models.Model):
    nome = models.CharField(db_column='Nome', max_length=50)  # Field name made lowercase.
    senha = models.CharField(max_length=50)
    email = models.CharField(max_length=80)
    com_code = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario'
