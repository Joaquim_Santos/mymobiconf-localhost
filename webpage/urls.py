from django.conf.urls import url

from . import views
from webpage.view import views_evento, views_atividade, views_questionario, views_opinioes, views_notas, views_patrocinadores, views_noticias, views_estatisticas, views_relatorio
from webpage.view.administracao import views_atividadeAdm, views_eventoAdm, views_noticiasAdm, views_patrocinadoresAdm, views_opinioesAdm, views_notasAdm, views_questionarioAdm, views_relatorioAdm

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^emailSucesso/$', views.enviarEmail, name='eEmail'),
    url(r'^sucesso/$', views.registroCompleto, name='registroCompleto'),
    url(r'^deslogar/$', views.deslogar, name='deslogar'),
    url(r'^logar/$', views.login, name='login'),
    url(r'^acessoInvalido/$', views.acessoInvalido, name='acesso_invalido'),
    url(r'^tutorial/$', views.tutorial, name='tutorial'),
    url(r'^control/$', views_evento.controlPanel, name='control_panel'),

    url(r'^administracao/$', views_estatisticas.estatisticas, name='administracao'),
    url(r'^administracao/eventos/$', views_estatisticas.eventosPesquisa, name='todos_eventos_pesquisa'),
    url(r'^administracao/publicado/$', views_estatisticas.publicadoApp, name='publicado_app'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/$', views_eventoAdm.detalheEventoAdm, name='detalhe_evento_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/editar/$', views_eventoAdm.editarEventoAdm, name='editar_evento_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/deletar/$', views_eventoAdm.deletarEventoAdm, name='deletar_evento_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/adicionar/$', views_eventoAdm.adicionarUsuarioAdm, name='adicionar_usuario_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/convidar/(?P<id_adm>[0-9]+)/$', views_eventoAdm.convidarUsuarioAdm, name='convidar_usuario_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/publicar/$', views_eventoAdm.publicarEventoAdm, name='publicar_evento_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/informacoes/$', views_eventoAdm.informacoesEventoAdm, name='informacoes_evento_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/(?P<id_usuario>[0-9]+)/deletar/$', views_eventoAdm.deletarUsuarioAdm, name='deletar_usuario_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/$', views_atividadeAdm.retornaListaAtividadesAdm, name='lista_atividades_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/novo/$', views_atividadeAdm.criarAtividadeAdm, name='atividade_novo_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/novo/completo/$', views_atividadeAdm.criarAtividadeAdm, name='atividade_novo_completo_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/$', views_atividadeAdm.detalhaAtividadeAdm, name='detalhe_atividade_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/editar/$', views_atividadeAdm.editaAtividadeAdm, name='editar_atividade_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/deletar/$', views_atividadeAdm.deletaAtividadeAdm, name='deletar_atividade_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/noticias/$', views_noticiasAdm.noticiasEventosAdm, name='lista_noticias_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/editar/$', views_noticiasAdm.editaNoticiaAdm, name='editar_noticia_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/$', views_noticiasAdm.detalhaNoticiaAdm, name='detalhe_noticia_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/noticias/novo/$', views_noticiasAdm.criarNoticiaAdm, name='nova_noticia_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/deletar/$', views_noticiasAdm.deletaNoticiaAdm, name='deletar_noticia_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/patrocinadores/$', views_patrocinadoresAdm.patrocinadoresEventosAdm, name='lista_patrocinadores_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/patrocinadores/novo/$', views_patrocinadoresAdm.adicionarPatrocinadorAdm, name='adicionar_patrocinador_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/patrocinadores/(?P<id_patrocinador>[0-9]+)/deletar/$', views_patrocinadoresAdm.deletarPatrocinadorAdm, name='deletar_patrocinador_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/patrocinadores/(?P<id_patrocinador>[0-9]+)/editar/$', views_patrocinadoresAdm.editarPatrocinadorAdm, name='editar_patrocinador_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/opinioes/$', views_opinioesAdm.opinioesEventosAdm, name='lista_opinioes_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/opinioes/$', views_opinioesAdm.estatisticasOpinioes, name='administracao_opinioes_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/notas/$', views_notasAdm.notasEventosAdm, name='painel_notas_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/eventoagoraAdm/$', views_notasAdm.EventoAgoraAdm, name='evento_agora_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/questionarios/$', views_questionarioAdm.questionariosEventosAdm, name='lista_questionarios_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/$', views_questionarioAdm.detalhaQuestionarioAdm, name='detalhe_questionario_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/questionarios/novo/$', views_questionarioAdm.novoQuestionarioAdm, name='novo_questionario_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/deletar/$', views_questionarioAdm.deletaQuestionarioAdm, name='deleta_questionario_adm'),
    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/editar/$', views_questionarioAdm.editarQuestionarioAdm, name='editar_questionario_adm'),

    url(r'^administracao/eventos/evento/(?P<id_evento>[0-9]+)/relatorio/$', views_relatorioAdm.menuRelatorioAdm, name='relatorio_adm'),

    url(r'^perfil/$', views.perfil, name='perfil'),
    url(r'^perfil/trocarSenha/$', views.trocarSenha, name='trocarSenha'),
    url(r'^perfil/editarPerfil/$', views.editarPerfil, name='editarPerfil'),
    url(r'^perfil/recuperarSenha/(?P<token>\w+)/$', views.recuperarSenha, name='recuperarSenha'), # Como deixar pronto pro GET?? (?P<token>)

    url(r'^control/evento/novo/$', views_evento.criarEvento, name='evento_novo'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/$', views_evento.detalheEvento, name='detalhe_evento'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/editar/$', views_evento.editarEvento, name='editar_evento'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/deletar/$', views_evento.deletarEvento, name='deletar_evento'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/adicionar/$', views_evento.adicionarUsuario, name='adicionar_usuario'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/convidar/(?P<id_adm>[0-9]+)/$', views_evento.convidarUsuario, name='convidar_usuario'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/(?P<id_usuario>[0-9]+)/deletar/$', views_evento.deletarUsuario, name='deletar_usuario'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/$', views_atividade.listaAtividades, name='lista_atividades'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/novo/$', views_atividade.criarAtividade, name='atividade_novo'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/novo/completo/$', views_atividade.criarAtividade, name='atividade_novo_completo'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/$', views_atividade.detalhaAtividade, name='detalhe_atividade'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/editar/$', views_atividade.editaAtividade, name='editar_atividade'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/atividade/(?P<id_atividade>[0-9]+)/deletar/$', views_atividade.deletaAtividade, name='deletar_atividade'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/informacoes/$', views_evento.informacoesEvento, name='informacoes_evento'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/meueventoagora/$', views_notas.meuEventoAgora, name='meu_evento_agora'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/notas/$', views_notas.retornaListaNotas, name='painel_notas'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/noticias/$', views_noticias.listaNoticias, name='lista_noticias'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/noticias/novo/$', views_noticias.criarNoticia, name='nova_noticia'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/$', views_noticias.detalhaNoticia, name='detalhe_noticia'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/editar/$', views_noticias.editaNoticia, name='editar_noticia'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/noticias/(?P<id_noticia>[0-9]+)/deletar/$', views_noticias.deletaNoticia, name='deletar_noticia'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/opinioes/$', views_opinioes.listaOpinioes, name='lista_opinioes'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/patrocinadores/$', views_patrocinadores.listaPatrocinadores, name='lista_patrocinadores'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/patrocinadores/novo/$', views_patrocinadores.adicionarPatrocinador, name='adicionar_patrocinador'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/patrocinadores/(?P<id_patrocinador>[0-9]+)/deletar/$', views_patrocinadores.deletarPatrocinador, name='deletar_patrocinador'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/patrocinadores/(?P<id_patrocinador>[0-9]+)/editar/$', views_patrocinadores.editarPatrocinador, name='editar_patrocinador'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/publicar/$', views_evento.publicarEvento, name='publicar_evento'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/questionarios/$', views_questionario.listaQuestionarios, name='lista_questionarios'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/questionarios/novo/$', views_questionario.novoQuestionario, name='novo_questionario'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/$', views_questionario.detalhaQuestionario, name='detalhe_questionario'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/deletar/$', views_questionario.deletaQuestionario, name='deleta_questionario'),
    url(r'^control/evento/(?P<id_evento>[0-9]+)/questionarios/(?P<id_questionario>[0-9]+)/editar/$', views_questionario.editarQuestionario, name='editar_questionario'),

    url(r'^control/evento/(?P<id_evento>[0-9]+)/relatorio/$', views_relatorio.menuRelatorio, name='relatorio')


]
