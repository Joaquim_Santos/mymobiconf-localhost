from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.decorators import usuario_pertence_evento
from webpage.controllers.administracao import questionarioAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def questionariosEventosAdm(request, id_evento):
    context = questionarioAdmCtrl.questionariosEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/questionarios/questionarios_estatisticas.html', context)

@login_required
@staff_member_required
def novoQuestionarioAdm(request, id_evento):
    context = questionarioAdmCtrl.novoQuestionarioAdm(request, id_evento)
    return render(request, 'administracao/eventos/questionarios/novo_questionario_estatisticas.html', context)

@login_required
@staff_member_required
def deletaQuestionarioAdm(request, id_evento, id_questionario):
    questionarioAdmCtrl.deletaQuestionarioAdm(request, id_questionario)
    return redirect('lista_questionarios_adm', id_evento=id_evento)

@login_required
@staff_member_required
def editarQuestionarioAdm(request, id_evento, id_questionario):
    context = questionarioAdmCtrl.editaQuestionarioAdm(request, id_evento, id_questionario)
    return render(request, 'administracao/eventos/questionarios/editar_questionario_estatisticas.html', context)

@login_required
@staff_member_required
def detalhaQuestionarioAdm(request, id_evento, id_questionario):
    context = questionarioAdmCtrl.detalhaQuestionarioAdm(request, id_evento, id_questionario)
    return render(request, 'administracao/eventos/questionarios/detalhe_questionario_estatisticas.html', context)
