from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers.administracao import patrocinadorAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def patrocinadoresEventosAdm(request, id_evento):
    context = patrocinadorAdmCtrl.patrocinadoresEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/patrocinadores/patrocinadores_estatisticas.html', context)

@login_required
@staff_member_required
def adicionarPatrocinadorAdm(request, id_evento):
    context = patrocinadorAdmCtrl.adicionarPatrocinadorAdm(request, id_evento)
    return render(request, 'administracao/eventos/patrocinadores/novo_patrocinador_estatisticas.html', context)

@login_required
@staff_member_required
def deletarPatrocinadorAdm(request, id_evento, id_patrocinador):
    patrocinadorAdmCtrl.deletarPatrocinadorAdm(request, id_patrocinador)
    return redirect('lista_patrocinadores_adm', id_evento=id_evento)

@login_required
@staff_member_required
def editarPatrocinadorAdm(request, id_evento, id_patrocinador):
    context = patrocinadorAdmCtrl.editarPatrocinadorAdm(request, id_evento, id_patrocinador)
    return render(request, 'administracao/eventos/patrocinadores/editar_patrocinador_estatisticas.html', context)
