from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from webpage.controllers.administracao import eventoAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def detalheEventoAdm(request, id_evento):
    context = eventoAdmCtrl.detalheEventoAdm(request, id_evento)
    return render(request, 'administracao/eventos/evento/detalhe_evento_estatisticas.html', context)

@login_required
@staff_member_required
def editarEventoAdm(request, id_evento):
    context = eventoAdmCtrl.editarEventoAdm(request, id_evento)
    return render(request, 'administracao/eventos/evento/editar_evento_estatisticas.html', context)

@login_required
@staff_member_required
def deletarEventoAdm(request, id_evento):
    eventoAdmCtrl.deletarEventoAdm(id_evento)
    return redirect('todos_eventos_pesquisa')

@login_required
@staff_member_required
def adicionarUsuarioAdm(request, id_evento):
    context1 = eventoAdmCtrl.adicionarUsuarioAdm(request, id_evento)
    context2 = eventoAdmCtrl.detalheEventoAdm(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'administracao/eventos/evento/detalhe_evento_estatisticas.html', context3)
    #eventoAdmCtrl.adicionarUsuarioAdm(request, id_evento)
    #return redirect('detalhe_evento_adm', id_evento=id_evento)

@login_required
@staff_member_required
def convidarUsuarioAdm(request, id_evento, id_adm):
    context1 = eventoAdmCtrl.convidarUsuarioAdm(request, id_evento, id_adm)
    context2 = eventoAdmCtrl.detalheEventoAdm(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'administracao/eventos/evento/detalhe_evento_estatisticas.html', context3)

@login_required
@staff_member_required
def deletarUsuarioAdm(request, id_evento, id_usuario):
    context1 = eventoAdmCtrl.deletarUsuarioAdm(request, id_evento, id_usuario)
    context2 = eventoAdmCtrl.detalheEventoAdm(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'eventos/detalhe_evento.html', context3)
    #return redirect('detalhe_evento_adm', id_evento=id_evento)

@login_required
@staff_member_required
def informacoesEventoAdm(request, id_evento):
    context = eventoAdmCtrl.informacoesEventoAdm(request, id_evento)
    return render(request, 'administracao/eventos/evento/editar_informacoes_estatisticas.html', context)

@login_required
@staff_member_required
def publicarEventoAdm(request, id_evento):
    eventoAdmCtrl.publicarEventoAdm(request, id_evento)
    return redirect('detalhe_evento_adm', id_evento=id_evento)
