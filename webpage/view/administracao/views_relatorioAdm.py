from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers import loginCtrl
from webpage.controllers.administracao import relatorioAdmCtrl
from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def menuRelatorioAdm(request, id_evento):
    context = relatorioAdmCtrl.menuRelatorioAdm(request, id_evento)
    return render(request, 'administracao/eventos/relatorio/detalhe_relatorio_estatisticas.html', context)
