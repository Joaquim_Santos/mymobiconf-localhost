from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers import loginCtrl
from webpage.controllers.administracao import noticiasAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required


@login_required
@staff_member_required
def noticiasEventosAdm(request, id_evento):
    context = noticiasAdmCtrl.noticiasEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/noticias/noticias_estatisticas.html', context)

@login_required
@staff_member_required
def editaNoticiaAdm(request, id_evento, id_noticia):
    context = noticiasAdmCtrl.editaNoticiaAdm(request, id_evento, id_noticia)
    return render(request, 'administracao/eventos/noticias/edita_noticia_estatisticas.html', context)

@login_required
@staff_member_required
def deletaNoticiaAdm(request, id_evento, id_noticia):
    noticiasAdmCtrl.deletaNoticiaAdm(request, id_noticia)
    return redirect('lista_noticias_adm', id_evento=id_evento)

@login_required
@staff_member_required
def criarNoticiaAdm(request, id_evento):
    context = noticiasAdmCtrl.adicionaNoticiaAdm(request, id_evento)
    return render(request, 'administracao/eventos/noticias/nova_noticia_estatisticas.html', context)

@login_required
@staff_member_required
def detalhaNoticiaAdm(request, id_evento, id_noticia):
    context = noticiasAdmCtrl.detalhaNoticiaAdm(request, id_evento, id_noticia)
    return render(request, 'administracao/eventos/noticias/detalhe_noticia_estatisticas.html', context)
