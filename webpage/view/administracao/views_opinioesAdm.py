from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from webpage.controllers.administracao import opiniaoAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required


@login_required
@staff_member_required
def opinioesEventosAdm(request, id_evento):
    context = opiniaoAdmCtrl.opinioesEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/opinioes/opinioes_estatisticas.html', context)

@login_required
@staff_member_required
def estatisticasOpinioes(request, id_evento):
    context = opiniaoAdmCtrl.estatisticasOpinioes(request, id_evento)
    return render(request, 'administracao/eventos/opinioes/opinioes_estatisticas.html', context)
