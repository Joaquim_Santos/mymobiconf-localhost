from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers.administracao import atividadeAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def retornaListaAtividadesAdm(request, id_evento):
    context = atividadeAdmCtrl.retornaListaAtividadesAdm(request,id_evento)
    return render(request, 'administracao/eventos/atividades/atividades_estatisticas.html', context)

@login_required
@staff_member_required
def detalhaAtividadeAdm(request, id_evento, id_atividade):
    context = atividadeAdmCtrl.detalhaAtividadeAdm(request, id_evento, id_atividade)
    return render(request, 'administracao/eventos/atividades/detalhe_atividade_estatisticas.html', context)

@login_required
@staff_member_required
def criarAtividadeAdm(request, id_evento):
    context = atividadeAdmCtrl.completaCadastroAtividadeAdm(request, id_evento)
    return render(request, 'administracao/eventos/atividades/nova_atividade_estatisticas.html', context)

@login_required
@staff_member_required
def editaAtividadeAdm(request, id_evento, id_atividade):
    context = atividadeAdmCtrl.editaAtividadeAdm(request, id_evento, id_atividade)
    return render(request, 'administracao/eventos/atividades/edita_atividade_estatisticas.html', context)

@login_required
@staff_member_required
def deletaAtividadeAdm(request, id_evento, id_atividade):
    atividadeAdmCtrl.deletaAtividadeAdm(request, id_atividade)
    return redirect('lista_atividades_adm', id_evento=id_evento)

@login_required
@staff_member_required
def notasEventosAdm(request, id_evento):
    context = atividadeAdmCtrl.notasEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/atividades/notas_atividades_estatisticas.html', context)
