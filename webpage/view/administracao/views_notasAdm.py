from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from webpage.controllers.administracao import notasAdmCtrl

from django.contrib.admin.views.decorators import staff_member_required

@staff_member_required
def notasEventosAdm(request, id_evento):
    context = notasAdmCtrl.notasEventosAdm(request, id_evento)
    return render(request, 'administracao/eventos/atividades/notas_atividades_estatisticas.html', context)

@login_required
@staff_member_required
def EventoAgoraAdm(request, id_evento):
    context = notasAdmCtrl.EventoAgoraAdm(request, id_evento)
    return render(request, 'administracao/eventos/atividades/evento_agora_estatisticas.html', context)
