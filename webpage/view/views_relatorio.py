from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers import loginCtrl, relatorioCtrl
from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def menuRelatorio(request, id_evento):
    context = relatorioCtrl.menuRelatorio(request, id_evento)
    return render(request, 'relatorio/detalhe_relatorio.html', context)
