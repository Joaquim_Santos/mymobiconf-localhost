from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from webpage.decorators import usuario_pertence_evento
from webpage.controllers import notasCtrl


@login_required
@usuario_pertence_evento
def retornaListaNotas(request, id_evento):
    context = notasCtrl.retornaListaNotas(request, id_evento)
    return render(request, 'notas/painel_notas.html', context)

@login_required
@usuario_pertence_evento
def meuEventoAgora(request, id_evento):
    context = notasCtrl.meuEventoAgora(request, id_evento)
    return render(request, 'notas/meu_evento_agora.html', context)