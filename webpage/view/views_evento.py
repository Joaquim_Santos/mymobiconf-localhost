from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from webpage.controllers import eventoCtrl
from webpage.decorators import usuario_pertence_evento

# Create your views here.

@login_required
def controlPanel(request):
    context = eventoCtrl.retornaListaEventos(request)
    return render(request, 'control_panel.html', context)

@login_required
def eventoPanel(request, id_evento):
    return render(request, 'index.html', {})

@login_required
def criarEvento(request):
    context = eventoCtrl.completaCadastroEvento(request)
    return render(request, 'eventos/novo_evento.html', context)

@login_required
@usuario_pertence_evento
def detalheEvento(request, id_evento):
    context = eventoCtrl.detalheEvento(request, id_evento)
    return render(request, 'eventos/detalhe_evento.html', context)

@login_required
@usuario_pertence_evento
def editarEvento(request, id_evento):
    context = eventoCtrl.editarEvento(request, id_evento)
    return render(request, 'eventos/editar_evento.html', context)

@login_required
@usuario_pertence_evento
def deletarEvento(request, id_evento):
    eventoCtrl.deletarEvento(id_evento)
    return redirect('control_panel')

@login_required
@usuario_pertence_evento
def adicionarUsuario(request, id_evento):
    context1 = eventoCtrl.adicionarUsuario(request, id_evento)
    context2 = eventoCtrl.detalheEvento(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'eventos/detalhe_evento.html', context3)

@login_required
@usuario_pertence_evento
def convidarUsuario(request, id_evento, id_adm):
    context1 = eventoCtrl.convidarUsuario(request, id_evento, id_adm)
    context2 = eventoCtrl.detalheEvento(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'eventos/detalhe_evento.html', context3)

@login_required
@usuario_pertence_evento
def deletarUsuario(request, id_evento, id_usuario):
    context1 = eventoCtrl.deletarUsuario(request, id_evento, id_usuario)
    context2 = eventoCtrl.detalheEvento(request, id_evento)
    context3 = {**context1, **context2} # Merge de dicionário
    return render(request, 'eventos/detalhe_evento.html', context3)
    #return redirect('detalhe_evento', id_evento=id_evento)

@login_required
@usuario_pertence_evento
def informacoesEvento(request, id_evento):
    context = eventoCtrl.informacoesEvento(request, id_evento)
    return render(request, 'eventos/editar_informacoes.html', context)

@login_required
@usuario_pertence_evento
def publicarEvento(request, id_evento):
    eventoCtrl.publicarEvento(request, id_evento)
    return redirect('detalhe_evento', id_evento=id_evento)
