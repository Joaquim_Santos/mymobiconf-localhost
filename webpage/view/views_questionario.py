from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from webpage.decorators import usuario_pertence_evento
from webpage.controllers import questionarioCtrl

# Create your views here.

@login_required
@usuario_pertence_evento
def listaQuestionarios(request, id_evento):
    context = questionarioCtrl.retornaListaQuestionarios(request, id_evento)
    return render(request, 'questionarios/questionarios.html', context)

@login_required
@usuario_pertence_evento
def detalhaQuestionario(request, id_evento, id_questionario):
    context = questionarioCtrl.detalhaQuestionario(request, id_evento, id_questionario)
    return render(request, 'questionarios/detalhe_questionario.html', context)

@login_required
@usuario_pertence_evento
def novoQuestionario(request, id_evento):
    context = questionarioCtrl.novoQuestionario(request, id_evento)
    return render(request, 'questionarios/novo_questionario.html', context)

@login_required
@usuario_pertence_evento
def deletaQuestionario(request, id_evento, id_questionario):
    questionarioCtrl.deletaQuestionario(request, id_questionario)
    return redirect('lista_questionarios', id_evento=id_evento)

@login_required
@usuario_pertence_evento
def editarQuestionario(request, id_evento, id_questionario):
    context = questionarioCtrl.editaQuestionario(request, id_evento, id_questionario)
    return render(request, 'questionarios/editar_questionario.html', context)