from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from webpage.controllers import patrocinadorCtrl
from webpage.decorators import usuario_pertence_evento

@login_required
@usuario_pertence_evento
def listaPatrocinadores(request, id_evento):
    context = patrocinadorCtrl.retornaListaPatrocinadores(request, id_evento)
    return render(request, 'patrocinadores/patrocinadores.html', context)

@login_required
@usuario_pertence_evento
def adicionarPatrocinador(request, id_evento):
    context = patrocinadorCtrl.adicionarPatrocinador(request, id_evento)
    return render(request, 'patrocinadores/novo_patrocinador.html', context)

@login_required
@usuario_pertence_evento
def deletarPatrocinador(request, id_evento, id_patrocinador):
    patrocinadorCtrl.deletarPatrocinador(request, id_patrocinador)
    return redirect('lista_patrocinadores', id_evento=id_evento)

@login_required
@usuario_pertence_evento
def editarPatrocinador(request, id_evento, id_patrocinador):
    context = patrocinadorCtrl.editarPatrocinador(request, id_evento, id_patrocinador)
    return render(request, 'patrocinadores/editar_patrocinador.html', context)
