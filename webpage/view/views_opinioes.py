from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from webpage.decorators import usuario_pertence_evento
from webpage.controllers import opiniaoCtrl
from aplicacaoSentimento import analisaSentimento, conexaoBD

@login_required
@usuario_pertence_evento
def listaOpinioes(request, id_evento):
    context = opiniaoCtrl.retornaListaOpinioes(request, id_evento)
    conexao = conexaoBD.operacoesBD()
    analisaSentimento.executar_analise()
    evento, quant_positivo, quant_negativo, quant_neutro = conexao.selecionar_score_evento(id_evento)
    if(evento is not None):
        if(evento['score_evento'] is not None):
            print('Score do evento e suas polaridades para a página de opiniões')
            print('Score do Evento: %.5f | Opiniões Positivas: %i | Opiniões Negativas: %i | Opiniões Neutras: %i' %(evento['score_evento'], quant_positivo['COUNT(*)'], quant_negativo['COUNT(*)'], quant_neutro['COUNT(*)']))
       
        else:
            print('Evento ainda não possui opiniões e score disponíveis')
            quant_positivo = quant_negativo = quant_neutro = 0
            sentimento = 'Não Disponível no Momento!'
            context.update({'sentimento': sentimento, 'quantPositivas': quant_positivo, 'quantNegativo':  quant_negativo, 'uantNeutro': quant_neutro})
            return render(request, 'opinioes/opinioes.html', context)

        if(evento['score_evento'] > 0.5):
            sentimento = 'Positivo'
        elif(evento['score_evento'] < 0.5 and evento['score_evento'] >= 0):
            sentimento = 'Neutro'
        else:
            sentimento = 'Negativo'
        context.update({'sentimento': sentimento, 'quantPositivas': quant_positivo['COUNT(*)'], 'quantNegativo': quant_negativo['COUNT(*)'],'uantNeutro': quant_neutro['COUNT(*)']})
        return render(request, 'opinioes/opinioes.html', context)
    else:
        print('Evento ainda não possui opiniões e score disponíveis')
        quant_positivo = quant_negativo = quant_neutro = 0
        sentimento = 'Não Disponível no Momento!'
        context.update({'sentimento': sentimento, 'quantPositivas': quant_positivo, 'quantNegativo': quant_negativo,'uantNeutro': quant_neutro})
        return render(request, 'opinioes/opinioes.html', context)
