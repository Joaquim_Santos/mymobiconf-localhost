from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from webpage.controllers import atividadeCtrl
from webpage.decorators import usuario_pertence_evento

# Create your views here.

@login_required
@usuario_pertence_evento
def listaAtividades(request, id_evento):
    context = atividadeCtrl.retornaListaAtividades(request,id_evento)
    return render(request, 'atividades/atividades.html', context)

@login_required
@usuario_pertence_evento
def criarAtividade(request, id_evento):
    context = atividadeCtrl.completaCadastroAtividade(request, id_evento)
    return render(request, 'atividades/nova_atividade.html', context)

@login_required
@usuario_pertence_evento
def detalhaAtividade(request, id_evento, id_atividade):
    context = atividadeCtrl.detalhaAtividade(request, id_evento, id_atividade)
    return render(request, 'atividades/detalhe_atividade.html', context)

@login_required
@usuario_pertence_evento
def editaAtividade(request, id_evento, id_atividade):
    context = atividadeCtrl.editaAtividade(request, id_evento, id_atividade)
    return render(request, 'atividades/edita_atividade.html', context)

@login_required
@usuario_pertence_evento
def deletaAtividade(request, id_evento, id_atividade):
    atividadeCtrl.deletaAtividade(request, id_atividade)
    return redirect('lista_atividades', id_evento=id_evento)