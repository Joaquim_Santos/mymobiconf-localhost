from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from webpage.decorators import usuario_pertence_evento
from webpage.controllers import noticiasCtrl

# Create your views here.

@login_required
@usuario_pertence_evento
def listaNoticias(request, id_evento):
    context = noticiasCtrl.retornaListaNoticias(request, id_evento)
    return render(request, 'noticias/noticias.html', context)

@login_required
@usuario_pertence_evento
def detalhaNoticia(request, id_evento, id_noticia):
    context = noticiasCtrl.detalhaNoticia(request, id_evento, id_noticia)
    return render(request, 'noticias/detalhe_noticia.html', context)

@login_required
@usuario_pertence_evento
def deletaNoticia(request, id_evento, id_noticia):
    noticiasCtrl.deletaNoticia(request, id_noticia)
    return redirect('lista_noticias', id_evento=id_evento)

@login_required
@usuario_pertence_evento
def criarNoticia(request, id_evento):
    context = noticiasCtrl.adicionaNoticia(request, id_evento)
    return render(request, 'noticias/nova_noticia.html', context)

@login_required
@usuario_pertence_evento
def editaNoticia(request, id_evento, id_noticia):
    context = noticiasCtrl.editaNoticia(request, id_evento, id_noticia)
    return render(request, 'noticias/edita_noticia.html', context)

