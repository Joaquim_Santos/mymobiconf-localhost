from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from webpage.controllers import loginCtrl, estatisticasCtrl
from webpage.controllers.administracao import atividadeAdmCtrl
from django.contrib.admin.views.decorators import staff_member_required

@login_required
@staff_member_required
def estatisticas(request):
    context = estatisticasCtrl.estatisticas(request)
    return render(request, 'administracao/estatisticas.html', context)

@login_required
@staff_member_required
def eventosPesquisa(request):
    context = estatisticasCtrl.eventosPesquisa(request)
    return render(request, 'administracao/eventos/lista_eventos_pesquisa.html', context)

@login_required
@staff_member_required
def publicadoApp(request):
    context = estatisticasCtrl.publicadoApp(request)
    return render(request, 'administracao/eventos/publicados_no_app.html', context)
