from django.core.exceptions import PermissionDenied
from webpage.models import EventoUsuario

def usuario_pertence_evento(function):
    def wrap(request, *args, **kwargs):
        eu = EventoUsuario.objects.filter(id_usuario=request.user.id, id_evento=kwargs['id_evento'])
        if eu.exists() or request.user.is_staff or request.user.is_superuser: #Permissão para utilizar super usuário!
            return function(request, *args, **kwargs)
        else:

            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
