from django.contrib import admin
from webpage.models import Atividade, Evento, Local, LocaisEvento, NotaAtividadeParticipante, NotaConfortoTermicoAtividade, NotaConfortoSonoroAtividade, Pergunta, Opiniao, Resposta, RespostaParticipante, Participante

# Register your models here.
class AtividadeAdmin(admin.ModelAdmin):
    list_display = ('nome', 'data')

admin.site.register(Atividade, AtividadeAdmin)
admin.site.register(Evento)
admin.site.register(Local)
admin.site.register(LocaisEvento)
admin.site.register(NotaAtividadeParticipante)
admin.site.register(NotaConfortoTermicoAtividade)
admin.site.register(NotaConfortoSonoroAtividade)
admin.site.register(Pergunta)
admin.site.register(Opiniao)
admin.site.register(Resposta)
admin.site.register(RespostaParticipante)
admin.site.register(Participante)

