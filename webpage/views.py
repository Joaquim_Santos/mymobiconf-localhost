from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from webpage.controllers import loginCtrl, eventoCtrl

# Create your views here.

def index(request):
    context = {'index': True}
    return render(request, 'index.html', context)

@login_required
def perfil(request):
    context = loginCtrl.perfil(request)
    return render(request, 'perfil/perfil.html', context)

@login_required
def trocarSenha(request): #(request, username)
    context = loginCtrl.trocarSenha(request) #(request, username)
    return render(request, 'perfil/trocar_senha.html', context)

def recuperarSenha(request, token): #(request, username)
    context = loginCtrl.recuperarSenha(request, token) #(request, username)
    return render(request, 'perfil/recuperar_senha.html', context)

def editarPerfil(request):
    context = loginCtrl.editarPerfil(request)
    return render(request, 'perfil/editar_perfil.html', context)

def enviarEmail(request):
    context = loginCtrl.enviarEmail(request)
    #return redirect('home')
    return render(request, 'index.html', context)

def registroCompleto(request):
    context = loginCtrl.validaCadastro(request)
    return render(request, 'index.html', context)

@login_required
def deslogar(request):
    loginCtrl.deslogar(request)
    #return render(request, 'index.html', context)
    return redirect('home')

def login(request):
    return loginCtrl.logar(request)

def acessoInvalido(request):
    return render(request, 'acesso_invalido.html', {})

@login_required
def tutorial(request):
    return render(request, 'tutorial.html', {})
