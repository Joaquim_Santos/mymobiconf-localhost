# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Avg, Max, Q
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from webpage.models import Evento, Atividade, Evento, Opiniao, Questionario, EventoUsuario, Participante, Patrocinador, Noticia, NotaAtividadeParticipante, NotaConfortoSonoroAtividade, NotaConfortoTermicoAtividade, Pergunta, Resposta
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.db.models import Count
from datetime import datetime, timedelta
import re

# Página de estatísticas, com médias e informações

def estatisticas(request):

    context = {}

    numU = User.objects.filter().count()
    numE = Evento.objects.filter().count()
    numA = Atividade.objects.filter().count()
    numQ = Questionario.objects.filter().count()
    numO = Opiniao.objects.filter().count()

    mediaEU = mediaFormatada(numE, numU)
    mediaAE = mediaFormatada(numA, numE)
    mediaQE = mediaFormatada(numQ, numE)
    mediaOE = mediaFormatada(numO, numE)

    ultE = Evento.objects.filter(privacidade__gte=0, privacidade__lte=2).order_by('-id').first()

    context = {'numU': numU, 'numE': numE, 'mediaEU': mediaEU, 'mediaAE': mediaAE, 'ultE': ultE, 'mediaQE': mediaQE, 'mediaOE': mediaOE}

    return context

def mediaFormatada(a, b):

    try:
        media = a/b
    except:
        media = 0

    media = str('{:.2f}'.format(media))
    media = media.replace('.', ',')

    return media

# Tela de todos os eventos (Pesquisa)

def eventosPesquisa(request):

    context = {}
    pesquisa = request.GET.get('pesquisa', '')

    if request.method == "GET":
        # __lte significa <= e __gte >=
        lista_eventos = Evento.objects.filter(privacidade__gte = 0, privacidade__lte = 3, nome__icontains=pesquisa) | Evento.objects.filter(privacidade__gte = 0, privacidade__lte = 3, sigla__icontains=pesquisa) # ('| ou lógico' faz o merge entre os QuerySets) e icontains é semelhante à Select ... like '%palavra%' sem Size Sensitive ('i' + contains)
        lista_eventos = lista_eventos.order_by('-data_inicio')
        qtdBusca = lista_eventos.count()
        #context = {'listaEventos' : lista_eventos, 'evento_usuario_lista_ids' : eu, 'qtdBusca': qtdBusca} SEM PAGINAÇÃO
    else:
        qtdBusca = Evento.objects.filter(privacidade__gte = 0, privacidade__lte = 3).count()
        lista_eventos = Evento.objects.all(privacidade__gte = 0, privacidade__lte = 3).order_by('-data_inicio')
        #context = {'listaEventos' : lista_eventos, 'evento_usuario_lista_ids' : eu, 'qtdBusca': qtdBusca} SEM PAGINAÇÃO

    if lista_eventos:
        vazio = False
    else:
        vazio = True

    page = request.GET.get('page', 1)

    paginator = Paginator(lista_eventos, 10)

    try:
        eventos = paginator.page(page)
    except PageNotAnInteger:
        eventos = paginator.page(1)
    except EmptyPage:
        eventos = paginator.page(paginator.num_pages)

    if eventos:
        qtd_items = len(eventos)
        lista_eventos = []
        lista = []
        lista_eve = {}
        for p in eventos:
            lista.append(p)
            '''if len(lista) == 3: "Parte responsável por dividir resultados em três colunas"
                lista_eve['qtd_eventos'] = 3
                lista_eve['eventos'] = lista
                lista = []
                lista_eventos.append(lista_eve)
                lista_eve = {}'''

        if len(lista) > 0:
            lista_eve['qtd_eventos'] = len(lista)
            lista_eve['eventos'] = lista
            lista_eventos.append(lista_eve)

    context = {"eventos": eventos, "listaEventos": lista_eventos ,"vazio" : vazio, 'qtdBusca': qtdBusca}#, "evento_usuario_lista_ids" : eu}

    return context

def publicadoApp(request):
    context = {}

    if request.method == "POST":
        listaPublicadosMarcados = request.POST.getlist('checkboxPublicados', None)
        if(listaPublicadosMarcados):
            # '-1' representa o checkbox "Selecionar todos", logo, não é um evento a ser removido do app
            if ('-1' in listaPublicadosMarcados):
                listaPublicadosMarcados.remove('-1')
            # Essa query faz um update em todos os IDs que estão na lista do checkBox
            Evento.objects.filter(id__in=listaPublicadosMarcados).update(ativo=0)

    dataAtual = datetime.now()
    dataUmMesAtras = dataAtual - timedelta(days=31) # Adotando mês como 31, máximo de dias existente em um mês
    lista_publicados = Evento.objects.filter(ativo = 1, data_fim__lte = dataUmMesAtras, privacidade__lte = 3)
    if lista_publicados:
        context.update({"lista_eventos_ativos": lista_publicados, "qtdBusca": lista_publicados.count()})
    else:
        context.update({"qtdBusca": 0})
    return context
