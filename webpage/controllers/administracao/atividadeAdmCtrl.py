from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Avg, Max
from webpage.models import Atividade, Evento, TipoAtividade, Local, LocaisEvento
from datetime import datetime
from datetime import timedelta

def retornaListaAtividadesAdm(request,id_evento):
    lista_atividades = Atividade.objects.filter(id_evento=id_evento).order_by('data','hora_inicio')
    page = request.GET.get('page', 1)

    paginator = Paginator(lista_atividades, 10)
    try:
        atividades = paginator.page(page)
    except PageNotAnInteger:
        atividades = paginator.page(1)
    except EmptyPage:
        atividades = paginator.page(paginator.num_pages)

    evento = Evento.objects.get(pk=id_evento)

    lista_data_ativ = []
    if atividades:
        prim_data = atividades[0].data;
        qtd_items = atividades.end_index() - atividades.start_index() + 1;
        lista = []
        for ativ in atividades:
            if ativ.data == prim_data:
                lista.append(ativ)
            else:
                data_ativ = {}
                data_ativ['data'] = prim_data
                data_ativ['atividades'] = lista
                lista_data_ativ.append(data_ativ)
                lista = []
                lista.append(ativ)
                prim_data = ativ.data

        data_ativ = {}
        data_ativ['data'] = prim_data
        data_ativ['atividades'] = lista
        lista_data_ativ.append(data_ativ)


    context = {'atividades': atividades, 'evento': evento, "lista_data_ativ" : lista_data_ativ}

    return context

def completaCadastroAtividadeAdm(request, id_evento):
    listaTipos = TipoAtividade.objects.filter().order_by('nome')
    context = {"sucesso": False, "listaTipos": listaTipos}
    if request.method == "POST":
        context.update({"cadastro_completo" : True})
        if request.POST['tema'] and request.POST['nome'] and request.POST['datainicio'] and request.POST['horainicio'] and request.POST['horafim'] and request.POST['tipo'] and request.POST['datafim']:
            if request.POST['horainicio'] <= request.POST['horafim']:
                if request.POST['datainicio'] <= request.POST['datafim']:
                    if verifySize(request):
                        tipo = TipoAtividade.objects.get(pk=request.POST['tipo'])
                        evento = Evento.objects.get(pk=id_evento)
                        local = Local(nome=request.POST['local'])
                        local.save()

                        dataInicio = datetime.strptime(request.POST['datainicio'], '%Y-%m-%d')
                        dataFim =  datetime.strptime(request.POST['datafim'], '%Y-%m-%d')
                        delta = dataFim - dataInicio

                        valorGrupo = Atividade.objects.all().aggregate(Max('grupo'))['grupo__max'] + 1

                        for x in range(0, delta.days+1):

                            a = Atividade(nome=request.POST['nome'], tema=request.POST['tema'])
                            a.data = dataInicio.strftime('%Y-%m-%d')
                            a.hora_inicio = request.POST['horainicio']
                            a.hora_fim = request.POST['horafim']
                            a.id_tipo = tipo
                            a.id_evento = evento
                            a.id_local = local
                            a.responsavel = request.POST['responsavel']
                            a.descricao = request.POST['descricao']

                            if delta.days > 0:
                                a.grupo = valorGrupo
                            else:
                                a.grupo = -1

                            a.save()

                            dataInicio = dataInicio + timedelta(days=1)

                        context.update({"sucesso" : True})

    context["id_evento"] = id_evento
    return context

def detalhaAtividadeAdm(request, id_evento, id_atividade):
    atividade = Atividade.objects.get(pk=id_atividade)
    nota_som = atividade.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
    nota_term = atividade.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
    nota = atividade.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0


    context = {"atividade" : atividade, "id_evento" : id_evento, "nota" : nota, "nota_term": nota_term, "nota_som": nota_som }
    return context

def editaAtividadeAdm(request, id_evento, id_atividade):
    context = {"sucesso": False}
    a = Atividade.objects.get(pk=id_atividade)
    #data = str(a.data)
    listaTipos = TipoAtividade.objects.filter().order_by('nome')

    if a.grupo != -1:
        listaDatas = Atividade.objects.filter(grupo=a.grupo).order_by('data').values_list('data', flat=True)
        data = str(listaDatas.first())
        dataUltima = str(listaDatas.last())
    else:
        data = str(a.data)
        dataUltima = data
    context.update({"atividade": a, "id_evento": id_evento, "data": data, "dataUltima": dataUltima})
    context["listaTipos"] = listaTipos

    if request.method == "POST":
        context.update({"cadastro_completo": True})
        if request.POST['tema'] and request.POST['nome'] and request.POST['datainicio'] and request.POST['horainicio'] and request.POST['horafim'] and request.POST['tipo'] and request.POST['local']:
            if request.POST['horainicio'] <= request.POST['horafim']:
                if verifySize(request):
                    tipo = TipoAtividade.objects.get(pk=request.POST['tipo'])
                    a = Atividade.objects.get(pk=id_atividade)

                    a.id_local.nome = request.POST['local']
                    a.id_local.save()
                    local = a.id_local
                    evento = a.id_evento
                    tipo = a.id_tipo


                    dataInicio = datetime.strptime(request.POST['datainicio'], '%Y-%m-%d')
                    dataFim =  datetime.strptime(request.POST['datafim'], '%Y-%m-%d')
                    delta = dataFim - dataInicio

                    if a.grupo != -1:
                        valorGrupoAntigo = a.grupo
                        if delta.days > 0:
                            valorGrupo = a.grupo
                        else:
                            valorGrupo = -1

                        listaDatasExistentes = list(Atividade.objects.filter(grupo=a.grupo).order_by('data').values_list('data', flat=True))
                        listaDatasNovas = []
                        for x in range(0, delta.days+1):
                            dataSoma = dataInicio + timedelta(days=x)
                            listaDatasNovas.append(dataSoma.date())

                        listaDatasIguais = list(set(listaDatasExistentes) &  set(listaDatasNovas))
                        listaDatasRemovidas = list(set(listaDatasExistentes) - set(listaDatasIguais))
                        listaDatasInserir = list(set(listaDatasNovas) - set(listaDatasIguais))


                        for x in range(0, len(listaDatasInserir)):
                            a = Atividade()
                            a = insereAtividade(request, a, listaDatasInserir[x], valorGrupo, local, evento, tipo)
                            ativBase = a

                        for x in range(0, len(listaDatasIguais)):
                            a = Atividade.objects.get(data = listaDatasIguais[x], grupo=valorGrupoAntigo)
                            a = insereAtividade(request, a, listaDatasIguais[x], valorGrupo, local, evento, tipo)
                            ativBase = a

                        if listaDatasRemovidas:
                            atividadesRemovidas = Atividade.objects.filter(data__in=listaDatasRemovidas, grupo=valorGrupoAntigo)
                            for ativ in atividadesRemovidas:
                                for quest in ativ.questionario_set.all():
                                    quest.id_atividade = ativBase
                                    quest.save()
                                ativ.delete()

                    else:
                        if delta.days > 0:
                            valorGrupo = Atividade.objects.all().aggregate(Max('grupo'))['grupo__max'] + 1
                        else:
                            valorGrupo = -1

                        for x in range(0, delta.days+1):
                            if delta.days > 0 and dataInicio.date() != a.data:
                                ativ = Atividade()
                                print("Dif")
                            else:
                                ativ = a
                                print("Igual")

                            ativ.nome = request.POST['nome']
                            ativ.tema = request.POST['tema']
                            ativ.data = dataInicio.strftime('%Y-%m-%d')
                            ativ.hora_inicio = request.POST['horainicio']
                            ativ.hora_fim = request.POST['horafim']
                            ativ.id_tipo = tipo
                            ativ.id_evento = evento
                            ativ.id_local = local
                            ativ.responsavel = request.POST['responsavel']
                            ativ.descricao = request.POST['descricao']
                            ativ.grupo = valorGrupo

                            ativ.save()

                            dataInicio = dataInicio + timedelta(days=1)



                    context.update({"sucesso": True})


    return context

def deletaAtividadeAdm(request, id_atividade):
    if request.method == "POST":
        a = Atividade.objects.get(pk=id_atividade)
        if a.grupo != -1:
            listaAtividades = Atividade.objects.filter(grupo = a.grupo)
            for ativ in listaAtividades:
                ativ.delete()
        else:
            a.delete()


def verifySize(request):

    tema = request.POST['tema']
    nome = request.POST['nome']
    descricao = request.POST['descricao']
    responsavel = request.POST['responsavel']

    tam_tema = Atividade._meta.get_field('tema').max_length
    tam_nome = Atividade._meta.get_field('nome').max_length
    tam_descricao = Atividade._meta.get_field('descricao').max_length
    tam_responsavel = Atividade._meta.get_field('responsavel').max_length

    if len(tema) <= tam_tema:
        if len(nome) <= tam_nome:
            if len(descricao) <= tam_descricao:
                if len(responsavel) <= tam_responsavel:
                    return True

    return False

def insereAtividadeAdm(request, a, dataAtiv, valorGrupo, local, evento, tipo):
    a.nome = request.POST['nome']
    a.tema = request.POST['tema']
    #a.data = request.POST['datainicio']
    a.data = dataAtiv.strftime('%Y-%m-%d')
    a.hora_inicio = request.POST['horainicio']
    a.hora_fim = request.POST['horafim']
    a.id_tipo = tipo
    a.responsavel = request.POST['responsavel']
    a.descricao = request.POST['descricao']
    a.id_local = local
    a.id_evento = evento

    a.grupo = valorGrupo

    a.save()

    return a
