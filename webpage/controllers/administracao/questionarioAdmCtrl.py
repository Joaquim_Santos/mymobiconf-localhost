from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from webpage.models import Questionario, Evento, Pergunta, Resposta, Atividade

def questionariosEventosAdm(request, id_evento):
    lista_questionarios = Questionario.objects.filter(id_evento=id_evento).order_by("id_atividade__data", "id_atividade__hora_inicio")
    page = request.GET.get('page', 1)

    paginator = Paginator(lista_questionarios, 10)
    try:
        questionarios = paginator.page(page)
    except PageNotAnInteger:
        questionarios = paginator.page(1)
    except EmptyPage:
        questionarios = paginator.page(paginator.num_pages)

    evento = Evento.objects.get(pk=id_evento)

    context = {'questionarios': questionarios, 'evento': evento, "lista_questionarios" : lista_questionarios}

    #context = { 'atividades' : atividades , 'evento' : evento}

    return context


def detalhaQuestionarioAdm(request, id_evento, id_questionario):

    questionario = Questionario.objects.get(pk=id_questionario)
    evento = Evento.objects.get(pk=id_evento)

    perguntas = Pergunta.objects.filter(id_questionario=questionario.id)

    lista_perguntas = []
    for p in perguntas:
        respostas = Resposta.objects.filter(id_pergunta=p.id)

        totalRespostas = p.respostaparticipante_set.count()

        for r in respostas:
            r.qtdResposta = r.respostaparticipante_set.count()
            if r.qtdResposta > 0:
                r.porcentagem = int(round(r.qtdResposta*100/totalRespostas,0))
            else:
                r.porcentagem = 0
        lista_respostas = {}
        lista_respostas['pergunta'] = p
        lista_respostas['respostas'] = respostas

        lista_perguntas.append(lista_respostas)





    context = {'questionario': questionario, 'evento': evento, 'lista_perguntas': lista_perguntas}
    return context


def novoQuestionarioAdm(request, id_evento):
    context = {}

    listaAtividades = Atividade.objects.filter(id_evento=id_evento).order_by('nome', 'id')
    lista = []
    if listaAtividades:
        lista.append(listaAtividades[0])
        ativAnterior = listaAtividades[0].nome
        for ativ in listaAtividades:
            if ativ.nome != ativAnterior:
                lista.append(ativ)
                ativAnterior = ativ.nome

    listaAtividades = lista

    context.update({'id_evento': id_evento, 'listaAtividades': listaAtividades})
    if request.method == "POST":
        listaPerguntas = request.POST.getlist('pergunta')
        listaQtdResposta = request.POST.getlist('qtdResposta')
        listaRespostas = request.POST.getlist('resposta')
        listaAtividadesEscolhidas = request.POST.getlist('atividadeEscolhida')

        evento = Evento.objects.get(pk=id_evento)

        for atividadeEscolhida in listaAtividadesEscolhidas:
            if atividadeEscolhida != "-1":
                atividade = Atividade.objects.get(pk=atividadeEscolhida)
            else:
                atividade = None


            quest = Questionario(id_evento=evento,id_atividade=atividade, nome = request.POST.get('titulo'), disponivel=1)
            quest.save()

            cont = 0

            for i, p in enumerate(listaPerguntas):
                perg = Pergunta(pergunta=p,id_questionario=quest)
                perg.save()

                for r in listaRespostas[cont:cont+int(listaQtdResposta[i])]:
                    resp = Resposta(resposta=r,id_pergunta=perg)
                    resp.save()

                cont += int(listaQtdResposta[i])


        # context.update({'cadastro_completo':True, 'sucesso': True, 'listaPerguntas': listaPerguntas, 'listaQtdResposta': listaQtdResposta, 'listaRespostas': listaRespostas, 'atividadeEscolhida':atividadeEscolhida})
        context.update({'cadastro_completo':True, 'sucesso': True, 'atividadeEscolhida':atividadeEscolhida})

    return context

def deletaQuestionarioAdm(request, id_questionario):
    if request.method == "POST":
        q = Questionario.objects.get(pk=id_questionario)
        q.delete()

def editaQuestionarioAdm(request, id_evento, id_questionario):

    listaAtividades = Atividade.objects.filter(id_evento=id_evento)
    context = {'id_evento': id_evento, 'listaAtividades': listaAtividades, 'id_questionario': id_questionario, 'tem_resposta': False}
    q = Questionario.objects.get(pk=id_questionario)

    if request.method == "POST":



        listaPerguntas = request.POST.getlist('pergunta')
        listaQtdResposta = request.POST.getlist('qtdResposta')
        listaRespostas = request.POST.getlist('resposta')
        atividadeEscolhida = request.POST.get('atividadeEscolhida')

        evento = Evento.objects.get(pk=id_evento)

        if atividadeEscolhida != "-1":
            atividade = Atividade.objects.get(pk=atividadeEscolhida)
        else:
            atividade = None

        q.delete()
        quest = Questionario(id_evento=evento,id_atividade=atividade, nome = request.POST.get('titulo'), disponivel=1)
        quest.save()

        cont = 0

        for i, p in enumerate(listaPerguntas):
            perg = Pergunta(pergunta=p,id_questionario=quest)
            perg.save()

            for r in listaRespostas[cont:cont+int(listaQtdResposta[i])]:
                resp = Resposta(resposta=r,id_pergunta=perg)
                resp.save()

            cont += int(listaQtdResposta[i])

        context.update({'quest': q, 'cadastro_completo':True, 'sucesso': True})
    else:
        temResposta = False
        try:
            for resposta in q.pergunta_set.first().resposta_set.all():
                if resposta.respostaparticipante_set.count() > 0:
                    temResposta = True
                    break

            context.update({'tem_reposta': temResposta, 'quest': q})
        except:
            # Ocorre quando 'resposta_set' está vazio, ou seja, quando o questionário não possui perguntas e nem repostas
            # Pode ser resolvido retornando os dados que estão no 'context' até o momento
            return context

    return context
