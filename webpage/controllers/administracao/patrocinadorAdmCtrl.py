from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from webpage.models import Evento, Patrocinador

def patrocinadoresEventosAdm(request, id_evento):
    e = Evento.objects.get(pk=id_evento)

    lista_patrocinadores = Patrocinador.objects.filter(id_evento=id_evento).order_by('prioridade')

    if lista_patrocinadores:
        vazio = False
    else:
        vazio = True

    page = request.GET.get('page', 1)

    paginator = Paginator(lista_patrocinadores, 9)
    try:
        patrocinadores = paginator.page(page)
    except PageNotAnInteger:
        patrocinadores = paginator.page(1)
    except EmptyPage:
        patrocinadores = paginator.page(paginator.num_pages)

    if patrocinadores:
        qtd_items = len(patrocinadores)
        lista_patrocinadores = []
        lista = []
        lista_pat = {}
        for p in patrocinadores:
            lista.append(p)
            if len(lista) == 3:
                lista_pat['qtd_patrocinadores'] = 3
                lista_pat['patrocinadores'] = lista
                lista = []
                lista_patrocinadores.append(lista_pat)
                lista_pat = {}

        if len(lista) > 0:
            lista_pat['qtd_patrocinadores'] = len(lista)
            lista_pat['patrocinadores'] = lista
            lista_patrocinadores.append(lista_pat)


    context = {"evento": e , "patrocinadores": patrocinadores, "lista_patrocinadores": lista_patrocinadores ,"vazio" : vazio}
    return context

def adicionarPatrocinadorAdm(request, id_evento):
    e = Evento.objects.get(pk=id_evento)
    context = {"sucesso" : False, "evento": e}
    if request.method == "POST":
        context.update({"cadastro_completo" : True})
        if request.POST['nome'] and request.POST['urlimagem'] and request.POST['prioridade']:
            if verifySize(request):
                p = Patrocinador(nome=request.POST['nome'])
                if request.POST['url']:
                    p.url = request.POST['url']
                else:
                    p.url = ""
                p.urlimagem = request.POST['urlimagem']
                p.prioridade = request.POST['prioridade']
                p.id_evento = e
                p.save()

                context.update({"sucesso" : True})

    return context

def editarPatrocinadorAdm(request, id_evento, id_patrocinador):
    e = Evento.objects.get(pk=id_evento)
    p = Patrocinador.objects.get(pk=id_patrocinador)
    context = {"sucesso" : False, "evento": e, "patrocinador": p}
    if request.method == "POST":
        context.update({"cadastro_completo" : True})
        if request.POST['nome'] and request.POST['urlimagem'] and request.POST['prioridade']:
            if verifySize(request):
                p.nome = nome=request.POST['nome']
                if request.POST['url']:
                    p.url = request.POST['url']
                else:
                    p.url = ""
                p.urlimagem = request.POST['urlimagem']
                p.prioridade = request.POST['prioridade']
                p.save()

                context.update({"sucesso" : True})

    return context

def deletarPatrocinadorAdm(request, id_patrocinador):
    if request.method == "POST":
        p = Patrocinador.objects.get(pk=id_patrocinador)
        p.delete()


def verifySize(request):

    nome = request.POST['nome']
    url = request.POST['url']
    urlimagem = request.POST['urlimagem']

    tam_nome = Patrocinador._meta.get_field('nome').max_length
    tam_url = Patrocinador._meta.get_field('url').max_length
    tam_urlimagem = Patrocinador._meta.get_field('urlimagem').max_length

    if len(nome) <= tam_nome:
        if len(url) <= tam_url:
            if len(urlimagem) <= tam_urlimagem:
                return True

    return False
