from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Avg
from webpage.models import Atividade, Evento, Opiniao, NotaAtividadeParticipante, NotaConfortoSonoroAtividade, NotaConfortoTermicoAtividade, ParticipanteEvento
from datetime import datetime
from django.db.models import Count

def retornaListaNotas(request,id_evento):

    context = {}

    dataAtual = datetime.now().strftime('%Y-%m-%d')
    horaAtual = datetime.now().strftime('%H:%M:%S')
    lista_atividades_atuais = Atividade.objects.filter(id_evento=id_evento,data=dataAtual,hora_inicio__lte=horaAtual, hora_fim__gte=horaAtual).order_by('data','hora_inicio') or False

    if lista_atividades_atuais:
        lista_atual = []
        for ativ in lista_atividades_atuais:
            ativ.nota_som = ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota_term = ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota = ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

            lista_atual.append(ativ)

        lista_atividades_atuais = lista_atual

    context.update({"notas_atividades_atuais" : lista_atividades_atuais})


    lista_atividades = Atividade.objects.filter(id_evento=id_evento).order_by('data','hora_inicio')
    page = request.GET.get('page', 1)

    paginator = Paginator(lista_atividades, 10)
    try:
        atividades = paginator.page(page)
    except PageNotAnInteger:
        atividades = paginator.page(1)
    except EmptyPage:
        atividades = paginator.page(paginator.num_pages)

    evento = Evento.objects.get(pk=id_evento)

    lista_data_ativ = []
    if atividades:
        prim_data = atividades[0].data;
        qtd_items = atividades.end_index() - atividades.start_index() + 1;
        lista = []
        for ativ in atividades:
            ativ.nota_som = ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota_term = ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota = ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

            if ativ.data == prim_data:
                lista.append(ativ)
            else:
                data_ativ = {}
                data_ativ['data'] = prim_data
                data_ativ['atividades'] = lista
                lista_data_ativ.append(data_ativ)
                lista = []
                lista.append(ativ)
                prim_data = ativ.data

        data_ativ = {}
        data_ativ['data'] = prim_data
        data_ativ['atividades'] = lista
        lista_data_ativ.append(data_ativ)

    context.update({'atividades': atividades, 'evento': evento, "lista_data_ativ" : lista_data_ativ})
    estatisticasNotas(id_evento, context)

    #context = { 'atividades' : atividades , 'evento' : evento}

    return context

def meuEventoAgora(request, id_evento):
    context = {}

    evento = Evento.objects.get(pk=id_evento)
    dataAtual = datetime.now().strftime('%Y-%m-%d')
    horaAtual = datetime.now().strftime('%H:%M:%S')
    lista_atividades_atuais = Atividade.objects.filter(id_evento=id_evento,data=dataAtual,hora_inicio__lte=horaAtual, hora_fim__gte=horaAtual).order_by('data','hora_inicio') or False

    if lista_atividades_atuais:
        lista_atual = []
        for ativ in lista_atividades_atuais:
            ativ.nota_som = ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota_term = ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            ativ.nota = ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

            lista_atual.append(ativ)

        lista_atividades_atuais = lista_atual

    lista_opinioes = Opiniao.objects.filter(id_evento=id_evento).order_by('-id')[:20]

    context.update({"notas_atividades_atuais" : lista_atividades_atuais, "opinioes": lista_opinioes, "evento" : evento})
    return context

def estatisticasNotas(id_evento, context):

    dataAtual = datetime.now().strftime('%Y-%m-%d')
    horaAtual = datetime.now().strftime('%H:%M:%S')
    lista_atividades_atuais = Atividade.objects.filter(id_evento=id_evento).order_by('data','hora_inicio') or False

    qtdNotas = Atividade.objects.filter(id_evento=id_evento).count()

    notaSonora = 0
    notaTermica = 0
    notaAtividade = 0

    if lista_atividades_atuais:
        lista_atual = []
        for ativ in lista_atividades_atuais:
            notaSonora += ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            notaTermica += ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
            notaAtividade += ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

    qtdSub = 0
    qtdNotaSonora = 0
    qtdNotaTermica = 0
    qtdNotaAtividade = 0
    qtdNotaAtividade = NotaAtividadeParticipante.objects.filter(id_evento=id_evento).count()
    qtdNotaSonora = NotaConfortoSonoroAtividade.objects.filter(id_evento=id_evento).count()
    qtdNotaTermica = NotaConfortoTermicoAtividade.objects.filter(id_evento=id_evento).count()

    qtdSub = qtdNotaAtividade + qtdNotaSonora + qtdNotaTermica

    qtdParticipantes = 0
    # Participantes que responderam, calcula a média inteira porque pode ser que alguns não tenham respondidos as três avaliações
    # qtdParticipantes = NotaAtividadeParticipante.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
    # qtdParticipantes += NotaConfortoSonoroAtividade.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
    # qtdParticipantes += NotaConfortoTermicoAtividade.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
    # qtdParticipantes = int(qtdParticipantes/3)
    qtdParticipantes = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()

    mediaNotaSonora = 0
    mediaNotaTermica = 0
    mediaNotaAtividade = 0

    if(qtdNotas>0):
        mediaNotaSonora = notaSonora/qtdNotas
        mediaNotaTermica = notaTermica/qtdNotas
        mediaNotaAtividade = notaAtividade/qtdNotas

    mediaSub = 0
    if(qtdParticipantes>0):
        mediaSub = qtdSub/qtdParticipantes

    context.update({"qtdNotaSonora" : qtdNotaSonora, "qtdNotaTermica" : qtdNotaTermica, "qtdNotaAtividade" : qtdNotaAtividade, "mediaSonora" : mediaNotaSonora, "mediaTermica": mediaNotaTermica, "mediaAtividade": mediaNotaAtividade, "numSubmissoes": qtdSub, "numParticipantesAvaliacao": qtdParticipantes, "mediaSubmissoes": mediaSub})
