from webpage.models import EventoUsuario, Evento, InformacaoEvento, Atividade, Questionario, Patrocinador, Noticia, ParticipanteEvento
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
import re
import threading

def retornaListaEventos(request):
    listaEventos = {}
    eu = EventoUsuario.objects.filter(id_usuario=request.user.id).values_list('id_evento', flat=True)
    eventos = Evento.objects.filter(pk__in=set(eu)).order_by('-data_inicio')
    listaEventos = {'listaEventos' : eventos}
    return listaEventos

def completaCadastroEvento(request):

    context = {"sucesso" : False}

    if request.method == "POST":
        print("Teste = "+ request.POST["privacidade"])
        context.update({"cadastro_completo" : True})
        if request.POST['sigla'] and request.POST['nome'] and request.POST['datainicio'] and request.POST['datafim']:
            if request.POST['datainicio'] <= request.POST['datafim']:
                e = Evento(sigla=request.POST['sigla'], nome=request.POST['nome'])
                e.informacoes_gerais = request.POST['descricao']
                e.data_inicio = request.POST['datainicio']
                e.data_fim = request.POST['datafim']
                e.urllogo = request.POST['urlLogo']
                e.local_principal = request.POST['local_principal']
                e.privacidade = request.POST['privacidade']
                e.ativo = 1

                e.save()
                eu = EventoUsuario(id_usuario=request.user.id, id_evento=e)
                eu.save()

                dataInicioFormatada = formataData(request.POST['datainicio'])
                dataFimFormatada = formataData(request.POST['datafim'])

                subject, from_email = 'Recebemos um novo evento :)', 'mymobiconf2018@gmail.com'
                text_content = ''
                html_content = '<p> Olá, recebemos mais um evento no sistema! </p> </br>'
                html_content += '<p> Sigla: '+ request.POST['sigla'] + '<br/>Nome: '+ request.POST['nome']
                html_content += '<br/>Informações do evento: ' + request.POST['descricao'] + '<br/>Início: '+ dataInicioFormatada + '<br/>Fim: '+ dataFimFormatada
                html_content += '<br/>Local: '+ request.POST['local_principal'] + '</p> </br>'
                html_content += '<p> Este é um e-mail automático, não é preciso respondê-lo!</p>' # favor não responder!?

                administradores = User.objects.filter(is_staff=1)

                destinatarios = "" # Inicializando a string

                for ad in administradores:
                    destinatarios += str(ad.email) + ","

                destinatarios = destinatarios[0:len(destinatarios)-1] # Removendo a ',' concatenada no final
                destinatarios = destinatarios.split(",")

                t = threading.Thread(target=threadingEnviaEmails,args=(subject, from_email, destinatarios, text_content, html_content))
                t.start()
                t._stop

                # Não é eficiente!!!
                '''for ad in administradores:
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [str(ad.email)])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()'''

                context.update({"sucesso" : True})

    return context

def threadingEnviaEmails(subject, from_email, destinatarios, text_content, html_content):
    try:
        msg = EmailMultiAlternatives(subject, text_content, from_email, bcc=destinatarios) # Esse bcc impede que os destinatários vejam quem recebeu o e-mail também
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    except:
        print("Erro ao enviar e-mail")

def formataData(dataPadrao):
    dataPadrao = str(dataPadrao)
    dataFormatada = dataPadrao[8:10] + '/' + dataPadrao[5:7] + '/' + dataPadrao[0:4]
    return dataFormatada

def detalheEvento(request, id_evento):
    evento = Evento.objects.get(pk=id_evento)
    eu = EventoUsuario.objects.values_list('id_usuario', flat= True).filter(id_evento=id_evento)
    informacao = InformacaoEvento.objects.filter(id_evento=id_evento).first()
    usu = User.objects.filter(pk__in=list(eu))
    print("Entrou aqui" + str(eu))
    context = {"evento" : evento, "listaUsuario": usu, "informacoes": informacao, "evento_usuario_lista_ids": eu}

    estatisticasEvento(id_evento, context)

    return context

def estatisticasEvento(id_evento, context):

    usuariosEvento = EventoUsuario.objects.filter(id_evento=id_evento).count()
    participantesEvento = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()
    atividadesEvento = Atividade.objects.filter(id_evento=id_evento).count()
    questionariosEvento = Questionario.objects.filter(id_evento=id_evento).count()
    patrocinadoresEvento = Patrocinador.objects.filter(id_evento=id_evento).count()
    noticiasEvento = Noticia.objects.filter(id_evento=id_evento).count()

    context.update({'numUsuarios': usuariosEvento, 'numParticipantes': participantesEvento, 'numAtividades': atividadesEvento,
    'numQuestionarios': questionariosEvento, 'numPatrocinadores': patrocinadoresEvento, 'numNoticias': noticiasEvento})

def editarEvento(request, id_evento):
    e = Evento.objects.get(pk=id_evento)
    context = {"evento" : e}

    if request.method == "POST":
        context.update({"cadastro_completo": True})
        if request.POST['sigla'] and request.POST['nome'] and request.POST['datainicio'] and request.POST['datafim']:
            if request.POST['datainicio'] <= request.POST['datafim']:
                e.nome = request.POST['nome']
                e.sigla = request.POST['sigla']
                e.informacoes_gerais = request.POST['descricao']
                e.data_inicio = request.POST['datainicio']
                e.data_fim = request.POST['datafim']
                e.urllogo = request.POST['urlLogo']
                e.local_principal = request.POST['local_principal']
                e.privacidade = request.POST['privacidade']

                e.save()
                context.update({"sucesso": True})

    return context

def deletarEvento(id_evento):
    e = Evento.objects.get(pk=id_evento)
    e.delete()

def adicionarUsuario(request, id_evento):
    context = {}
    try:
        usu = User.objects.get(email=request.POST['email'])
        e = Evento.objects.get(pk=id_evento)
        cadastrado = EventoUsuario.objects.filter(id_usuario = usu.id, id_evento=e).count()
        if(cadastrado == 0):
            eu = EventoUsuario(id_usuario = usu.id, id_evento=e)
            eu.save()
            context = {'usuario_administrador': True, 'sucesso_adm': True}
        else:
            context = {'usuario_administrador': True, 'adm_ja_cadastrado': True}
    except:
        context = {'usuario_administrador': True, 'sucesso_adm': False}

    return context

def deletarUsuario(request, id_evento, id_usuario):
    context = {}
    try:
        e = Evento.objects.get(pk=id_evento)
        eu = EventoUsuario.objects.get(id_usuario = id_usuario, id_evento = e)
        eu.delete()
        context = {'deleta_adm': True, 'adm_removido': True}
    except:
        context = {'deleta_adm': True, 'adm_removido': False}
    return context

def informacoesEvento(request, id_evento):
    e = Evento.objects.get(pk=id_evento)
    context = {"evento" : e}

    try:
        informacoes = InformacaoEvento.objects.get(id_evento=id_evento)
    except InformacaoEvento.DoesNotExist:
        informacoes = InformacaoEvento(id_evento=e, site= "", email = "", telefone="")
        informacoes.save()
    context.update({'informacoes' : informacoes})
    if request.method == "POST":
        context.update({"cadastro_completo": True})

        addressToVerify = request.POST['email']
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', addressToVerify)

        if match == None and request.POST['email']:
            context.update({"sucesso": False, "erro": "Email inválido."})
        else:
            if request.POST['site']:
                informacoes.site = request.POST['site']
            if request.POST['email']:
                informacoes.email = request.POST['email']
            if request.POST['telefone']:
                informacoes.telefone = request.POST['telefone']

            informacoes.save()
            context.update({"sucesso": True})


    return context

def publicarEvento(request, id_evento):

    if request.method == "POST":
        e = Evento.objects.get(pk=id_evento)

        e.ativo = 1 - e.ativo # Inverte de 0 para 1 e 1 para 0
        e.save()

def convidarUsuario(request, id_evento, id_adm):
    context = {}
    try:
        user = User.objects.get(pk=id_adm)
        evento = Evento.objects.get(pk=id_evento)

        if request.method == "POST" and user!=None:
            #Enviar e-mail com link

            subject, from_email, to = 'Convite para o sistema do MyMobiConf', 'mymobiconf2018@gmail.com', request.POST['email']
            text_content = ''
            html_content = '<p> Olá, ' + user.first_name + ' está te convidando para fazer parte da equipe organizadora do evento ' + evento.nome + '.</p> '
            html_content += '<p> Acesse o link para se cadastrar: </p> <a href="http://www.mymobiconf.caf.ufv.br/?cadastrar=true"> Link para cadastro </a>'
            html_content += '<p> Após terminar o cadastro, procure informá-lo para que possa te adicionar ao evento.</p>'
            html_content += '<p> Aqui está o endereço de e-mail desse administrador: ' + user.email + '</p>'

            t = threading.Thread(target=threadingEnviaEmails,args=(subject, from_email, [to], text_content, html_content))
            t.start()
            t._stop

            context.update({'sucessoConvite': True})

        else:
            context.update({'sucessoConvite': False})

    except Exception as e:

        context.update({'sucessoConvite': False})

    return context

def threadingEnviaEmails(subject, from_email, to, text_content, html_content):
    msg = EmailMultiAlternatives(subject, text_content, from_email, bcc=to)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
