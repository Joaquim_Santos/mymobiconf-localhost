from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from webpage.models import Evento, Opiniao, EventoUsuario, ParticipanteEvento
from django.db.models import Count
import re

def retornaListaOpinioes(request, id_evento):

    palavraschave = request.GET.get('palavraschave', None)

    if palavraschave is not None:

        palavras = palavraschave.split(';')
        opcao = int(request.GET.get('opcaobusca'))
        q_list = Q()

        # Contenha uma ou mais palavras chaves == 1
        if opcao == 1:
            for p in palavras:
                q_list.add(Q(opiniao__icontains=p), Q.OR)
        else:
            #Contenha todas as palavras chaves == 2
            for p in palavras:
                q_list.add(Q(opiniao__icontains=p), Q.AND)



        lista_opinioes = Opiniao.objects.filter(q_list, id_evento=id_evento).order_by('-id')

    else:
        lista_opinioes = Opiniao.objects.filter(id_evento=id_evento).order_by('-id')

    if lista_opinioes:
        vazio = False
    else:
        vazio = True

    page = request.GET.get('page', 1)

    qtdopinioes = request.GET.get('qtd', 20)

    try:
        qtdopinioes = int(qtdopinioes)
    except:
        qtdopinioes = 20

    if qtdopinioes < 1:
        qtdopinioes = 20

    paginator = Paginator(lista_opinioes, qtdopinioes)
    try:
        opinioes = paginator.page(page)
    except PageNotAnInteger:
        opinioes = paginator.page(1)
    except EmptyPage:
        opinioes = paginator.page(paginator.num_pages)

    evento = Evento.objects.get(pk=id_evento)

    #WordCloud
    wordcloud = request.GET.get('wordcloud',False)

    context = {'opinioes' : opinioes, 'evento' : evento, 'vazio': vazio, 'wordcloud': wordcloud}
    if wordcloud:
        lista_opinioes = Opiniao.objects.filter(id_evento=id_evento)
        texto = ""

        peso_wordcloud = int(request.GET.get('peso_wordcloud',3))
        if peso_wordcloud < 0:
            peso_wordcloud=3;

        for opi in lista_opinioes:
            texto = texto + " " + opi.opiniao

        cloud = word_count(texto)
        context.update({'peso_opinioes': cloud,'peso_wordcloud': peso_wordcloud})

    totalOp, mediaPO, qtdMaisO, qtdMenosO, numParticipantes = estatisticasOpinioes(id_evento)
    context.update({'totalOpinioes': totalOp, 'mediaPartOpi': mediaPO, 'qtdMaisOpinioes': qtdMaisO,'qtdMenosOpinioes': qtdMenosO, 'qtdParticipantes': numParticipantes})

    return context

def estatisticasOpinioes(id_evento):

    opiniao = Opiniao.objects.filter(id_evento = id_evento)
    #evento = Evento.objects.filter(id = id_evento)
    totalOp = opiniao.count()

    if(totalOp > 0):

        countOpinioes = opiniao.values('id_participante').annotate(total=Count('id_participante'))
        usuarioMaisOpinou = countOpinioes.order_by('-total').first()
        usuarioMenosOpinou = countOpinioes.order_by('total').first()

        qtdMaisOpinioes = usuarioMaisOpinou['total']
        qtdMenosOpinioes = usuarioMenosOpinou['total']

        numParticipantes = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()
        mediaPO = mediaFormatada(totalOp, numParticipantes)

        return (totalOp, mediaPO, qtdMaisOpinioes, qtdMenosOpinioes, numParticipantes)
    else:
        numParticipantes = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()
        return (totalOp, 0, 0, 0, numParticipantes)

def mediaFormatada(a, b):

    try:
        media = a/b
    except:
        media = 0

    media = str('{:.2f}'.format(media))
    media = media.replace('.', ',')

    return media


def word_count(str):
    counts = dict()
    words = str.split()
    stopWords = getStopWords()

    for word in words:
        if not word.isalnum():
            word = re.sub('\W+','', word)
        if word.lower() not in stopWords:
            if word.lower() in counts:
                counts[word.lower()]['count'] += 1
            else:
                counts[word.lower()] = {}
                counts[word.lower()]['count'] = 1
                counts[word.lower()]['word'] = word
    return counts

def getStopWords():

    words = [
        'a','agora','ainda','alguém','algum',
        'alguma','algumas','alguns','ampla','amplas',
        'amplo','amplos','ante','antes','ao',
        'aos','após','aquela','aquelas','aquele',
        'aqueles','aquilo','as','até','através','b','c',
        'cada','coisa','coisas','com','como',
        'contra','contudo','da','daquele','daqueles','d',
        'das','de','dela','delas','dele',
        'deles','depois','dessa','dessas','desse',
        'desses','desta','destas','deste','deste',
        'destes','deve','devem','devendo','dever',
        'deverá','deverão','deveria','deveriam','devia',
        'deviam','disse','disso','disto','dito',
        'diz','dizem','do','dos','e',
        'é','ela','elas','ele','eles',
        'em','enquanto','entre','era','essa',
        'essas','esse','esses','esta','está',
        'estamos','estão','estas','estava','estavam',
        'estávamos','este','estes','estou','eu','f',
        'fazendo','fazer','feita','feitas','feito',
        'feitos','foi','for','foram','fosse','g','i','j','h',
        'fossem','grande','grandes','há','isso',
        'isto','já','la','lá','lhe','k','l',
        'lhes','lo','mas','me','mesma','m',
        'mesmas','mesmo','mesmos','meu','meus',
        'minha','minhas','muita','muitas','muito',
        'muitos','na','não','nas','nem','n',
        'nenhum','nessa','nessas','nesta','nestas',
        'ninguém','no','nos','nós','nossa',
        'nossas','nosso','nossos','num','numa',
        'nunca','o','os','ou','outra',
        'outras','outro','outros','para','pela','p',
        'pelas','pelo','pelos','pequena','pequenas',
        'pequeno','pequenos','per','perante','pode',
        'pude','podendo','poder','poderia','poderiam',
        'podia','podiam','pois','por','porém',
        'porque','posso','pouca','poucas','pouco',
        'poucos','primeiro','primeiros','própria','próprias',
        'próprio','próprios','quais','qual','quando',
        'quanto','quantos','que','quem','são','q','r',
        'se','seja','sejam','sem','sempre','s',
        'sendo','será','serão','seu','seus',
        'si','sido','só','sob','sobre',
        'sua','suas','talvez','também','tampouco','t',
        'te','tem','tendo','tenha','ter',
        'teu','teus','ti','tido','tinha',
        'tinham','toda','todas','todavia','todo',
        'todos','tu','tua','tuas','tudo',
        'última','últimas','último','últimos','um','u',
        'uma','umas','uns','vendo','ver',
        'vez','vindo','vir','vos','vós','v','w','x','y','z',
        'tô','deu','hj','dia','aí','vc','faz','ne','eh',
        '1','2','3','4','5','6','7','8','9','0','to',
        'tá'
    ]


    return words


# def retornaListaAtividades(request,id_evento):
#     lista_atividades = Atividade.objects.filter(id_evento=id_evento).order_by('data')
#     page = request.GET.get('page', 1)

#     paginator = Paginator(lista_atividades, 10)
#     try:
#         atividades = paginator.page(page)
#     except PageNotAnInteger:
#         atividades = paginator.page(1)
#     except EmptyPage:
#         atividades = paginator.page(paginator.num_pages)

#     evento = Evento.objects.get(pk=id_evento)

#     lista_data_ativ = []
#     if atividades:
#         prim_data = atividades[0].data;
#         qtd_items = atividades.end_index() - atividades.start_index() + 1;
#         lista = []
#         for ativ in atividades:
#             if ativ.data == prim_data:
#                 lista.append(ativ)
#             else:
#                 data_ativ = {}
#                 data_ativ['data'] = prim_data
#                 data_ativ['atividades'] = lista
#                 lista_data_ativ.append(data_ativ)
#                 lista = []
#                 lista.append(ativ)
#                 prim_data = ativ.data

#         data_ativ = {}
#         data_ativ['data'] = prim_data
#         data_ativ['atividades'] = lista
#         lista_data_ativ.append(data_ativ)

#     context = {'atividades': atividades, 'evento': evento, "lista_data_ativ" : lista_data_ativ}

#     #context = { 'atividades' : atividades , 'evento' : evento}

#     return context
