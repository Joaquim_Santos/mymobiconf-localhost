from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Avg, Max, Q
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from webpage.models import Evento, Atividade, Evento, Opiniao, Questionario, EventoUsuario, Participante, Patrocinador, Noticia, NotaAtividadeParticipante, NotaConfortoSonoroAtividade, NotaConfortoTermicoAtividade, Pergunta, Resposta, ParticipanteEvento
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.db.models import Count
from datetime import datetime
import re

def menuRelatorio(request, id_evento):

    evento = Evento.objects.get(pk=id_evento)
    context = {'evento': evento}
    eventoRelatorio(request, id_evento, context)
    eventoRelatorioQuestionario(request, id_evento, context)
    eventoRelatorioOpinioes(request, id_evento, context)
    eventoRelatorioWordCloud(request, id_evento, context)

    return context

def eventoRelatorio(request, id_evento, context):

    itensMarcados = request.POST.getlist('checkboxRelatorio', None)
    context.update({'checkboxMarcadosRelatorio': itensMarcados})
    evento = Evento.objects.get(pk=id_evento)
    listaQuestionarios = Questionario.objects.filter(id_evento=id_evento).order_by("id_atividade__data", "id_atividade__hora_inicio")

    if(len(itensMarcados) > 0):
        context.update({'relatorioDisponivel': True})

    if('1' in itensMarcados):

        usuariosEvento = EventoUsuario.objects.filter(id_evento=id_evento).count()
        participantesEvento = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()
        atividadesEvento = Atividade.objects.filter(id_evento=id_evento).count()
        questionariosEvento = Questionario.objects.filter(id_evento=id_evento).count()
        patrocinadoresEvento = Patrocinador.objects.filter(id_evento=id_evento).count()
        noticiasEvento = Noticia.objects.filter(id_evento=id_evento).count()

        context.update({'exibir1': True, 'numUsuarios': usuariosEvento, 'numParticipantes': participantesEvento, 'numAtividades': atividadesEvento,
        'numQuestionarios': questionariosEvento, 'numPatrocinadores': patrocinadoresEvento, 'numNoticias': noticiasEvento})

    if('2' in itensMarcados):

        opiniao = Opiniao.objects.filter(id_evento = id_evento)
        totalOp = opiniao.count()

        if(totalOp > 0):
            countOpinioes = opiniao.values('id_participante').annotate(total=Count('id_participante'))
            usuarioMaisOpinou = countOpinioes.order_by('-total').first()
            usuarioMenosOpinou = countOpinioes.order_by('total').first()
            qtdMaisOpinioes = usuarioMaisOpinou['total']
            qtdMenosOpinioes = usuarioMenosOpinou['total']
        else:
            countOpinioes = 0
            usuarioMaisOpinou = 0
            usuarioMenosOpinou = 0
            qtdMaisOpinioes = 0
            qtdMenosOpinioes = 0

        numParticipantes = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()

        mediaOp = 0
        if (numParticipantes > 0):
            mediaOp = totalOp/numParticipantes


        context.update({'exibir2': True, 'totalOpinioes': totalOp, 'mediaOpiniao': mediaOp, 'usuarioMaisOpinou': qtdMaisOpinioes, 'usuarioMenosOpinou': qtdMenosOpinioes, 'numParticipantesOpiniao': numParticipantes})

    if('3' in itensMarcados):

        dataAtual = datetime.now().strftime('%Y-%m-%d')
        horaAtual = datetime.now().strftime('%H:%M:%S')
        lista_atividades_atuais = Atividade.objects.filter(id_evento=id_evento).order_by('data','hora_inicio') or False

        qtdNotas = Atividade.objects.filter(id_evento=id_evento).count()

        notaSonora = 0
        notaTermica = 0
        notaAtividade = 0

        if lista_atividades_atuais:
            lista_atual = []
            for ativ in lista_atividades_atuais:
                notaSonora += ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
                notaTermica += ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
                notaAtividade += ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

        qtdNotaAtividade = NotaAtividadeParticipante.objects.filter(id_evento=id_evento).count()
        qtdNotaSonora = NotaConfortoSonoroAtividade.objects.filter(id_evento=id_evento).count()
        qtdNotaTermica = NotaConfortoTermicoAtividade.objects.filter(id_evento=id_evento).count()

        qtdSub = qtdNotaAtividade + qtdNotaSonora + qtdNotaTermica

        qtdParticipantes = 0
        # Participantes que responderam, calcula a média inteira porque pode ser que alguns não tenham respondidos as três avaliações
        # qtdParticipantes = NotaAtividadeParticipante.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
        # qtdParticipantes += NotaConfortoSonoroAtividade.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
        # qtdParticipantes += NotaConfortoTermicoAtividade.objects.filter(id_evento=id_evento).values('id_participante').annotate(total=Count('id_participante')).count()
        # qtdParticipantes = int(qtdParticipantes/3)
        qtdParticipantes = ParticipanteEvento.objects.filter(id_evento=id_evento).values('id_participante').distinct().count()

        mediaNotaSonora = 0
        mediaNotaTermica = 0
        mediaNotaAtividade = 0

        if(qtdNotas>0):
            mediaNotaSonora = notaSonora/qtdNotas
            mediaNotaTermica = notaTermica/qtdNotas
            mediaNotaAtividade = notaAtividade/qtdNotas

        mediaSub = 0
        if(qtdParticipantes>0):
            mediaSub = qtdSub/qtdParticipantes

        context.update({"exibir3": True, "qtdNotaSonora" : qtdNotaSonora, "qtdNotaTermica" : qtdNotaTermica, "qtdNotaAtividade" : qtdNotaAtividade,
        "mediaSonora" : mediaNotaSonora, "mediaTermica": mediaNotaTermica, "mediaAtividade": mediaNotaAtividade, "numSubmissoes": qtdSub, "numParticipantesAvaliacao": qtdParticipantes, "mediaSubmissoes": mediaSub})

    if('4' in itensMarcados):

        dataAtual = datetime.now().strftime('%Y-%m-%d')
        horaAtual = datetime.now().strftime('%H:%M:%S')
        lista_atividades_atuais = Atividade.objects.filter(id_evento=id_evento).order_by('data','hora_inicio') or False

        qtdNotas = Atividade.objects.filter(id_evento=id_evento).count()

        notaSonora = 0
        notaTermica = 0
        notaAtividade = 0

        if lista_atividades_atuais:
            lista_atual = []
            for ativ in lista_atividades_atuais:
                notaSonora += ativ.notaconfortosonoroatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
                notaTermica += ativ.notaconfortotermicoatividade_set.aggregate(Avg('nota'))['nota__avg'] or 0
                notaAtividade += ativ.notaatividadeparticipante_set.aggregate(Avg('nota'))['nota__avg'] or 0

        mediaNotaSonoraGrafica = 0
        mediaNotaTermicaGrafica = 0
        mediaNotaAtividadeGrafica = 0

        if(qtdNotas>0):
            mediaNotaSonoraGrafica = notaSonora/qtdNotas
            mediaNotaTermicaGrafica = notaTermica/qtdNotas
            mediaNotaAtividadeGrafica = notaAtividade/qtdNotas

        context.update({"exibir4": True, "mediaSonoraGrafica" : mediaNotaSonoraGrafica, "mediaTermicaGrafica": mediaNotaTermicaGrafica, "mediaAtividadeGrafica": mediaNotaAtividadeGrafica})

    return context

def eventoRelatorioQuestionario(request, id_evento, context):

    itensMarcados = request.POST.getlist('checkboxQuestionarioRelatorio', None)
    context.update({'checkboxMarcadosQuestionario': itensMarcados})
    listaQuestionarios = Questionario.objects.filter(id_evento=id_evento).order_by("id_atividade__data", "id_atividade__hora_inicio")

    if(len(itensMarcados) > 0):

        listaQuestionariosResposta = []
        for i in range(0, len(itensMarcados)):
            resultado = Questionario.objects.filter(id_evento=id_evento, id=itensMarcados[i]).order_by("id_atividade__data", "id_atividade__hora_inicio")
            if resultado:
                listaQuestionariosResposta += resultado

        lista_perguntas_total = []
        lista_questionarios_e_atividades = []
        lista_respostas_total = []

        for q in listaQuestionariosResposta:

            perguntas = Pergunta.objects.filter(id_questionario=q.id)
            lista_questionarios_e_atividades.append([q.nome, q.id_atividade])

            lista_perguntas = []
            for p in perguntas:
                respostas = Resposta.objects.filter(id_pergunta=p.id)

                totalRespostas = p.respostaparticipante_set.count()

                for r in respostas:
                    r.qtdResposta = r.respostaparticipante_set.count()
                    if r.qtdResposta > 0:
                        r.porcentagem = int(round(r.qtdResposta*100/totalRespostas,0))
                    else:
                        r.porcentagem = 0
                lista_respostas = {}
                lista_respostas['pergunta'] = p
                lista_respostas['respostas'] = respostas

                lista_perguntas.append(lista_respostas)
            #IDENTAÇÃO COM QUESTIONARIOS
            lista_perguntas_total.append(lista_perguntas)


        context.update({'exibir5': True, 'relatorioDisponivel': True, 'questionario_e_atividade': lista_questionarios_e_atividades, 'lista_perguntas': lista_perguntas_total})

    context.update({"listaQuestionarios": listaQuestionarios})

    return context

def eventoRelatorioOpinioes(request, id_evento, context):

    palavraschave = request.POST.get('palavraschave')
    itemMarcado = request.POST.get('intervaloOpinioes')
    itemMarcadoFiltro = request.POST.get('intervaloOpinioesFiltro')
    context.update({'radiobuttonMarcadosOpinioes': itemMarcado, 'radiobuttonMarcadosOpinioesFiltro': itemMarcadoFiltro})

    try:
        itemMarcado = int(itemMarcado)
        itemMarcadoFiltro = int(itemMarcadoFiltro)
    except:
        return context

    if itemMarcado != 0:

        lista_opinioes = {}
        if palavraschave is not None:

            qtdOpinioes = 0

            if itemMarcado == 1:
                lista_opinioes = Opiniao.objects.filter(opiniao__icontains=palavraschave, id_evento=id_evento).order_by('-id')
                qtdOpinioes = len(lista_opinioes)
            elif itemMarcado == 2:
                lista_opinioes = Opiniao.objects.filter(opiniao__icontains=palavraschave, id_evento=id_evento).order_by('id')
                qtdOpinioes = len(lista_opinioes)
            elif itemMarcado == 3:
                qtdOpinioes = int(request.POST.get('qtdOpinioesNumber'))
                lista_opinioes = Opiniao.objects.filter(opiniao__icontains=palavraschave, id_evento=id_evento)#.order_by('-id')[0:qtdOpinioes]
                if(len(lista_opinioes) < qtdOpinioes):
                    qtdOpinioes = len(lista_opinioes)
                if(itemMarcadoFiltro == 1):
                    lista_opinioes = lista_opinioes.order_by('id')[0:qtdOpinioes]
                elif(itemMarcadoFiltro == 2):
                    lista_opinioes = lista_opinioes.order_by('-id')[0:qtdOpinioes]

        if(qtdOpinioes > 0):
            context.update({'relatorioDisponivel': True, 'exibir6': True, 'qtdOpinioes': qtdOpinioes, 'listaOpinioes': lista_opinioes, 'id_evento': id_evento})
        elif (qtdOpinioes == 0):
            context.update({'relatorioDisponivel': True, 'exibir6': True, 'qtdOpinioes': qtdOpinioes, 'id_evento': id_evento})

    return context


def eventoRelatorioWordCloud(request, id_evento, context):

    itensMarcados = request.POST.getlist('checkboxWordCloud', None)

    context.update({'checkboxMarcadosWordCloud': itensMarcados})
    if(len(itensMarcados) > 0):

        lista_opinioes = Opiniao.objects.filter(id_evento=id_evento)
        texto = ""

        peso_wordcloud = int(request.POST.get('pesoWordCloud'))
        context.update({'pesoPalavras': peso_wordcloud})

        for opi in lista_opinioes:
            texto = texto + " " + opi.opiniao

        cloud = word_count(texto)
        context.update({'exibir7': True, 'relatorioDisponivel': True, 'peso_opinioes': cloud,'peso_wordcloud': peso_wordcloud})
    else:
        context.update({'exibir7': False})

    return context

def word_count(str):
    counts = dict()
    words = str.split()
    stopWords = getStopWords()

    for word in words:
        if not word.isalnum():
            word = re.sub('\W+','', word)
        if word.lower() not in stopWords:
            if word.lower() in counts:
                counts[word.lower()]['count'] += 1
            else:
                counts[word.lower()] = {}
                counts[word.lower()]['count'] = 1
                counts[word.lower()]['word'] = word
    return counts

def getStopWords():

    words = [
        'a','agora','ainda','alguém','algum',
        'alguma','algumas','alguns','ampla','amplas',
        'amplo','amplos','ante','antes','ao',
        'aos','após','aquela','aquelas','aquele',
        'aqueles','aquilo','as','até','através','b','c',
        'cada','coisa','coisas','com','como',
        'contra','contudo','da','daquele','daqueles','d',
        'das','de','dela','delas','dele',
        'deles','depois','dessa','dessas','desse',
        'desses','desta','destas','deste','deste',
        'destes','deve','devem','devendo','dever',
        'deverá','deverão','deveria','deveriam','devia',
        'deviam','disse','disso','disto','dito',
        'diz','dizem','do','dos','e',
        'é','ela','elas','ele','eles',
        'em','enquanto','entre','era','essa',
        'essas','esse','esses','esta','está',
        'estamos','estão','estas','estava','estavam',
        'estávamos','este','estes','estou','eu','f',
        'fazendo','fazer','feita','feitas','feito',
        'feitos','foi','for','foram','fosse','g','i','j','h',
        'fossem','grande','grandes','há','isso',
        'isto','já','la','lá','lhe','k','l',
        'lhes','lo','mas','me','mesma','m',
        'mesmas','mesmo','mesmos','meu','meus',
        'minha','minhas','muita','muitas','muito',
        'muitos','na','não','nas','nem','n',
        'nenhum','nessa','nessas','nesta','nestas',
        'ninguém','no','nos','nós','nossa',
        'nossas','nosso','nossos','num','numa',
        'nunca','o','os','ou','outra',
        'outras','outro','outros','para','pela','p',
        'pelas','pelo','pelos','pequena','pequenas',
        'pequeno','pequenos','per','perante','pode',
        'pude','podendo','poder','poderia','poderiam',
        'podia','podiam','pois','por','porém',
        'porque','posso','pouca','poucas','pouco',
        'poucos','primeiro','primeiros','própria','próprias',
        'próprio','próprios','quais','qual','quando',
        'quanto','quantos','que','quem','são','q','r',
        'se','seja','sejam','sem','sempre','s',
        'sendo','será','serão','seu','seus',
        'si','sido','só','sob','sobre',
        'sua','suas','talvez','também','tampouco','t',
        'te','tem','tendo','tenha','ter',
        'teu','teus','ti','tido','tinha',
        'tinham','toda','todas','todavia','todo',
        'todos','tu','tua','tuas','tudo',
        'última','últimas','último','últimos','um','u',
        'uma','umas','uns','vendo','ver',
        'vez','vindo','vir','vos','vós','v','w','x','y','z',
        'tô','deu','hj','dia','aí','vc','faz','ne','eh',
        '1','2','3','4','5','6','7','8','9','0','to',
        'tá'
    ]


    return words
