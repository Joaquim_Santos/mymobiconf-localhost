# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
#from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
import random
import string
from django.template import Context
from datetime import datetime
from datetime import timedelta
from webpage.models import Token
import threading

def perfil(request):
    '''context = {}
    eu = User.objects.all()
    context = {'dadosPerfil' : eu}
    return context'''
    return

def trocarSenha(request):
    context = {}
    user = User.objects.get(username=request.user.username) # USUÁRIO POR USERNAME É ÚNICO

    if request.method == "POST":
        context.update({'cadastro_completo': True})
        if (user.check_password(request.POST['senha_antiga'])):
            if request.POST['nova_senha'] == request.POST['confirma_senha']:
                if (len(request.POST['nova_senha']) >=6 ):
                    user.set_password(request.POST['nova_senha'])
                    user.save()
                    login(request, user)
                    context.update({'sucesso': True})
                else:
                    context.update({'sucesso': False, 'erro': "Nova senha menor que 6 dígitos!"})
            else:
                context.update({'sucesso': False, 'erro': "Nova senha e senha de confirmação estão diferentes!"})
        else:
            context.update({'sucesso': False, 'erro': "Senha diferente do sistema!"})
    else:
        context.update({'sucesso': False, 'erro': "Dados incompletos!"})

    return context

def editarPerfil(request):
    context = {}
    user = User.objects.get(id=request.user.id) # USUÁRIO POR USERNAME É ÚNICO

    if request.method == "POST":
        if request.POST['nome'] != '' and request.POST['email'] != '':
            context.update({'cadastro_completo': True})
            user.first_name = request.POST['nome']
            user.email = request.POST['email']
            user.save()
            context.update({'sucesso': True})
    else:
        context.update({'sucesso': False, 'erro': "Dados incompletos!"})

    return context

def enviarEmail(request):
    context = {'index': True, 'retornoEmail': True}
    try:
        user = User.objects.get(email=request.POST['email'])

        if request.method == "POST" and user!=None:

            userValidation = Token.objects.filter(id = user.id).count()

            if(userValidation == 1):

                userToken = Token.objects.get(id = user.id)

                token = ''.join(random.choice(string.ascii_letters) for xA in range(120)) + str(user.id) # Token que contém apenas letras + id do usuário (Único), com isso, a exclusividade do token é garantida

                now = datetime.now()
                dataToken = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(now.hour) + ":" + str(now.minute) + ":" + str(now.second)[:2]

                userToken.tokenValidade = dataToken # Atualiza campos
                userToken.tokenRecuperar = token
                userToken.save()

                #Enviar e-mail com link

                subject, from_email, to = 'Recuperação de Senha (MyMobiConf)', 'mymobiconf2018@gmail.com', request.POST['email']
                text_content = ''
                html_content = '<p> Olá, acesse o link para alterar sua senha: </p> </br> <a href="http://www.mymobiconf.caf.ufv.br/perfil/recuperarSenha/' + token + '"> Link para alteração </a>' + '<p> Caso não tenha sido você que solicitou essa troca de senha, ignore esse e-mail!</p>'

                t = threading.Thread(target=threadingEnviaEmails,args=(subject, from_email, [to], text_content, html_content))
                t.start()
                t._stop

                context.update({'sucesso': True})

            else:

                token = ''.join(random.choice(string.ascii_letters) for xA in range(120)) + str(user.id) # Token que contém apenas letras + id do usuário (Único), com isso, a exclusividade do token é garantida

                now = datetime.now()
                dataToken = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(now.hour) + ":" + str(now.minute) + ":" + str(now.second)[:2]
                userToken = Token(id = user.id, tokenRecuperar = token, tokenValidade = dataToken) # Insere campos

                userToken.save()

                #Enviar e-mail com link
                subject, from_email, to = 'Recuperação de Senha (MyMobiConf)', 'mymobiconf2018@gmail.com', request.POST['email']
                text_content = ''
                html_content = '<p> Olá, acesse o link para alterar sua senha: </p> </br> <a href="http://www.mymobiconf.caf.ufv.br/perfil/recuperarSenha/' + token + '"> Link para alteração </a>' + '<p> Caso não tenha sido você que solicitou essa troca de senha, ignore esse e-mail!</p>'

                t = threading.Thread(target=threadingEnviaEmails,args=(subject, from_email, [to], text_content, html_content))
                t.start()
                t._stop

                context.update({'sucesso': True})

        else:
            context.update({'sucesso': False})

    except Exception as e:

        context.update({'sucesso': False})

    return context

def threadingEnviaEmails(subject, from_email, destinatarios, text_content, html_content):
    msg = EmailMultiAlternatives(subject, text_content, from_email, bcc=destinatarios)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def recuperarSenha(request, token):
    context = {}
    #tokenExiste = Token.objects.filter(tokenRecuperar=token).count() # a diferença de get e filter é que o get "cria um objeto" que pode receber alterações

    #if(tokenExiste==1):
    try:

        tokenAux = Token.objects.get(tokenRecuperar=token)
        user = User.objects.get(id=tokenAux.id)

        padraoData = '%Y-%m-%d %H:%M:%S'
        # dataInicial = str(tokenAux.tokenValidade)[:19]
        dataInicial = str((tokenAux.tokenValidade - timedelta(hours=3)))[:19]
        dataFinal = str(datetime.now())[:19]

        deltaSegundos = (datetime.strptime(dataFinal, padraoData) - datetime.strptime(dataInicial, padraoData)).total_seconds()

        if(deltaSegundos <= (60*15)): # Token é válido por apenas 15 minutos
            if request.method == "POST" and user!=None:
                context.update({'cadastro_completo': True})
                if request.POST['n_senha'] == request.POST['c_senha']:
                    if (len(request.POST['n_senha']) >=6 ):
                        user.set_password(request.POST['n_senha'])
                        user.save()
                        login(request, user)
                        context.update({'sucesso': True})
                        #return redirect('control_panel')
                    else:
                        context.update({'sucesso': False, 'erro': "Nova senha menor que 6 dígitos!"})
                else:
                        context.update({'sucesso': False, 'erro': "Nova senha e senha de confirmação estão diferentes!"})
            else:
                context.update({'sucesso': False, 'erro': "Dados incompletos!"})
        else:
                context.update({'sucesso': False, 'erro': "Esse token não é válido mais,\nsolicite outro na tela de login!", 'tokenInvalidoInicializacao': True})

    except Exception as e:

        context.update({'sucesso': False, 'erro': "Esse token não é válido mais,\nsolicite outro na tela de login!", 'tokenInvalidoInicializacao': True})

    return context


def validaCadastro(request):
    c = {'index': True, 'registroCompleto': True}

    if request.method == "POST":
        if request.POST['nome'] and request.POST['user'] and request.POST['email']:
            if len(request.POST['pass']) >=6 and request.POST['pass'] == request.POST['pass2']:
                try:
                    if not (User.objects.filter(email=request.POST['email']).exists()):
                        if not (User.objects.filter(username=request.POST['user']).exists()):
                            user =  User.objects.create_user(request.POST['user'], request.POST['email'], request.POST['pass'])
                            user.first_name = request.POST['nome']
                            user.save()

                            user = authenticate(request, username=request.POST['user'], password=request.POST['pass'])
                            if user is not None:

                                # Adicionar função de enviar e-mail

                                subject, from_email = 'Recebemos um novo usuário :)', 'mymobiconf2018@gmail.com'#, request.POST['email']
                                text_content = ''
                                html_content = '<p> Olá, recebemos mais um usuário no sistema! </p> </br>'
                                html_content += '<p> Nome: '+ request.POST['nome'] + '<br/>Username: '+ request.POST['user']
                                html_content += '<br/>E-mail: '+ request.POST['email'] + '</p> </br>'
                                html_content += '<p> Este é um e-mail automático, não é preciso respondê-lo!</p>'

                                administradores = User.objects.filter(is_staff=1)


                                destinatarios = "" # Inicializando a string

                                for ad in administradores:
                                    destinatarios += str(ad.email) + ","

                                destinatarios = destinatarios[0:len(destinatarios)-1] # Removendo a ',' concatenada no final
                                destinatarios = destinatarios.split(",")

                                #msg = EmailMultiAlternatives(subject, text_content, from_email, bcc=destinatarios) # Esse bcc impede que os destinatários vejam quem recebeu o e-mail também
                                #msg.attach_alternative(html_content, "text/html")
                                #msg.send()

                                t = threading.Thread(target=threadingEnviaEmails,args=(subject, from_email, destinatarios, text_content, html_content))
                                t.start()
                                t._stop

                                # Não é eficiente!!!
                                '''for ad in administradores:
                                    msg = EmailMultiAlternatives(subject, text_content, from_email, [str(ad.email)])
                                    msg.attach_alternative(html_content, "text/html")
                                    msg.send()'''

                                login(request, user)

                                c.update({'sucesso': True})
                            else:
                                c.update({'sucesso': False, 'erro': "Bug ao logar"})
                        else:
                            c.update({'sucesso': False, 'erro': "Nome de usuário já existente"})
                    else:
                        c.update({'sucesso': False, 'erro': "Email já existente"})
                except Exception as e:
                    c.update({'sucesso': False, 'erro': e})
            else:
                c.update({'sucesso': False, 'erro': "Senhas não coincidem ou tem tamanho menor que 6"})
        else:
            c.update({'sucesso': False, 'erro': "Por favor, preencha todos os dados"})
    return c

def deslogar(request):
    #context = {'index': True, 'logout': True}

    logout(request)

def logar(request):
    try:
        user = authenticate(request, username=request.POST['user'], password=request.POST['pass'])
        if user is not None:
            login(request, user)
            return redirect('control_panel')
        else:
            c = {'index': True, 'loginError': True}
            c.update({'erro': 'Usuário inexistente!'})
            return render(request, 'index.html', c)
    except Exception as e:
        c = {'index': True, 'loginError': True, 'erro': e}
        return render(request, 'index.html', c)
