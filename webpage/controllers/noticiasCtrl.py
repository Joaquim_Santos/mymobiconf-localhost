from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from webpage.models import Noticia, Evento

def retornaListaNoticias(request, id_evento):
    lista_noticias = Noticia.objects.filter(id_evento=id_evento).order_by("-hora")
    page = request.GET.get('page', 1)

    paginator = Paginator(lista_noticias, 10)
    try:
        noticias = paginator.page(page)
    except PageNotAnInteger:
        noticias = paginator.page(1)
    except EmptyPage:
        noticias = paginator.page(paginator.num_pages)

    evento = Evento.objects.get(pk=id_evento)

    context = {'noticias': noticias, 'evento': evento, "lista_noticias" : lista_noticias}

    return context

def detalhaNoticia(request, id_evento, id_noticia):
    evento = Evento.objects.get(pk=id_evento)

    noticia = Noticia.objects.get(pk=id_noticia)

    context = {'noticia': noticia, 'evento': evento}
    return context

def deletaNoticia(request, id_noticia):
    if request.method == "POST":
        noticia = Noticia.objects.get(pk = id_noticia)
        noticia.delete()

def adicionaNoticia(request, id_evento):
    e = Evento.objects.get(pk=id_evento)
    context = {"sucesso" : False, "evento": e}
    if request.method == "POST":
        context.update({"cadastro_completo" : True})
        if request.POST['titulo'] and request.POST['data'] and request.POST['hora'] and request.POST['corpo']:
            if verifySize(request):
                n = Noticia(titulo= request.POST['titulo'])
                n.hora = request.POST['data'] + " " + request.POST['hora']
                n.corpo = request.POST['corpo']
                n.id_evento = e
                n.save()

                context.update({"sucesso" : True})

    return context

def editaNoticia(request, id_evento, id_noticia):
    e = Evento.objects.get(pk=id_evento)
    n = Noticia.objects.get(pk=id_noticia)
    context = {"sucesso" : False, "evento": e}
    if request.method == "POST":
        context.update({"cadastro_completo" : True})
        if request.POST['titulo'] and request.POST['data'] and request.POST['hora'] and request.POST['corpo']:
            if verifySize(request):
            
                n.titulo = request.POST['titulo']
                n.hora = request.POST['data'] + " " + request.POST['hora']
                n.corpo = request.POST['corpo']
                n.save()

                context.update({"sucesso" : True})
                
    context.update({"noticia": n})
    return context

def verifySize(request):
    
    titulo = request.POST['titulo']
    corpo = request.POST['corpo']

    tam_titulo = Noticia._meta.get_field('titulo').max_length
    tam_corpo = Noticia._meta.get_field('corpo').max_length


    if len(titulo) <= tam_titulo:
        if len(corpo) <= tam_corpo:
            return True

    return False