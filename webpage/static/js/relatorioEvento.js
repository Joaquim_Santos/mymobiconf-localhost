window.onload = function(){
    //habilitaBotao();
    habilitaTextNumber();
    mudaEstado('quest', 'setaQuestionario');
    mudaEstado('opinioes', 'setaOpinioes');
    mudaEstado('wordcloud', 'setaWordCloud');
    verifica();
}

/*function habilitaBotao() {
    var qtdMarcado = $('input[name=checkboxRelatorio]:checked').length;
    alert(qtdMarcado);
    if(qtdMarcado > 0)
        $(':input[name=geraRelatorio]').prop('disabled', false);
    else
        $(':input[name=geraRelatorio]').prop('disabled', true);
}*/

function marcarCheckboxs(nomeCheckbox){
    checkboxes = document.getElementsByName(nomeCheckbox);
    var i;
    if(!checkboxes[0].checked){
        for(i = 0; i < checkboxes.length; i++)
           checkboxes[i].checked = false;
    } else {
        for(i = 0; i < checkboxes.length; i++)
            checkboxes[i].checked = true;
    }
}

function mudaEstado(el, idSeta) {

    var display = document.getElementById(el).style.display;
    var seta = document.getElementById(idSeta);

    if(display == 'none') {
        seta.className = "glyphicon glyphicon-chevron-up";
        seta.title="Esconder";
        document.getElementById(el).style.display = 'block';
    }
    else {
        seta.className = "glyphicon glyphicon-chevron-down";
        seta.title="Exibir";
        document.getElementById(el).style.display = 'none';
    }
}

function habilitaTextNumber(){

    var radioButton = document.getElementsByName('intervaloOpinioes');
    var valorQtdOpinioes = document.getElementById('maximoOpinioes');
    var divRadioQuantidade = document.getElementById('ordemQuantidade');

    if(radioButton[3].checked == true) {
        valorQtdOpinioes.style.display = 'block';
        divRadioQuantidade.style.display = 'block';
        valorQtdOpinioes.disabled = "";
        if(valorQtdOpinioes.value == "")
            valorQtdOpinioes.value = "1";
        valorQtdOpinioes.focus();
    } else {
        valorQtdOpinioes.value = "";
        valorQtdOpinioes.style.display = 'none';
        divRadioQuantidade.style.display = 'none';
    }
}

function verifica(){
    var slider = document.getElementById("sliderWordCloud");
    var output = document.getElementById("valorExibicaoWC");
    output.innerHTML = slider.value;

    slider.oninput = function() {
      output.innerHTML = this.value;
    }
}
