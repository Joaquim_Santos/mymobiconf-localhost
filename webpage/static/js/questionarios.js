$('#btnAdd').click(function() {
    var qtdRespostas = $("#qtdRespostas").val();
    var i;
    
    var respostas = '';
    for (i = 0; i < qtdRespostas; i++) {
        respostas += '<div class="form-group"><input type="text" class="form-control" maxlength="150" name="resposta" placeholder="Resposta" data-match-error="Preencha este campo" required><div class="help-block with-errors"></div></div>'
    }
    if(qtdRespostas >=2 && qtdRespostas<=10 ) {
        $('#botaoPergunta').before('<div class="questionario well" style="margin:20px;"><input type="hidden" name="qtdResposta" value="'+qtdRespostas+'"></input><div class="form-group"><label class="control-label">Pergunta </label><a class="remove" name="remove"><span class="glyphicon glyphicon-remove"></span>Deletar Pergunta</a><input type="text" maxlength="250" class="form-control" name="pergunta" placeholder="Pergunta" data-match-error="Preencha este campo" required><div class="help-block with-errors"></div></div><label class="control-label">Respostas</label><a class="aumentaResp" name="aumentaResp"><span class="glyphicon glyphicon-remove"></span>Adicionar Resposta</a><a class="removeResp" name="removeResp"><span class="glyphicon glyphicon-remove"></span>Remover resposta</a>' + respostas+'</div>');
    }
});

$(document).on("click", ".remove", function() {
    $(this).parent().parent().remove();
});

$(document).on("click", ".aumentaResp", function() {

    var qtdRespostas = parseInt($(this).siblings("input[name=qtdResposta]").val());
    qtdRespostas += 1;
    $(this).siblings("input[name=qtdResposta]").val(qtdRespostas);

    $(this).siblings(".form-group:last").after('<div class="form-group"><input type="text" maxlength="150" class="form-control" name="resposta" placeholder="Resposta" data-match-error="Preencha este campo" required><div class="help-block with-errors"></div></div>')
});

$(document).on("click", ".removeResp", function() {

    var qtdRespostas = parseInt($(this).siblings("input[name=qtdResposta]").val());
    qtdRespostas -= 1;

    if(qtdRespostas==1) {
        $(this).parent().remove();
    } else {
        $(this).siblings("input[name=qtdResposta]").val(qtdRespostas);
        $(this).siblings(".form-group:last").remove();
    }
    
});


