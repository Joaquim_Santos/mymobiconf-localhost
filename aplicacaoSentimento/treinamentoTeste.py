import pandas as pd
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sn
import sys

def ler_entrada_treino():
    try:
        dataset = pd.read_csv('aplicacaoSentimento/opinioes_usuarios.csv')
        opinioes = dataset['Opinião'].values
        classes = dataset['Sentimento'].values
    except Exception as excecao:
        print("Ocorreu uma exceção ao ler o arquivo")
        print(f"Tipo da exceção: {type(excecao)}")
        print("Descrição da exceção:\n\n")
        print(excecao)
        sys.exit()

    '''
    tweets = []
    classes = []
    with open("opinioes_usuarios.txt", "rt") as f:
        for frase in f.read().split('\n'):
            frase = frase.split('---')
            tweets.append(frase[1])
            classes.append(frase[2])
    '''

    return opinioes, classes

def construir_modelo_sentimento(listaFrases, classes):
    listaFrases = numpy.array(listaFrases)
    classes = numpy.array(classes)
    vectorizer = CountVectorizer(ngram_range=(1, 1))
    freq_tweets = vectorizer.fit_transform(listaFrases)
    modelo = MultinomialNB(alpha=3.0)
    modelo.fit(freq_tweets, classes)

    return modelo, vectorizer, freq_tweets

def avaliar_desempenho(modelo, frequencia_frases, classes):
    resultados = cross_val_predict(modelo, frequencia_frases, classes, cv=10)
    precisao = metrics.accuracy_score(classes, resultados)
    print(f"A  precisão do mmodelo treinado com a base em questão é {precisao*100}%\n")

    sentimentos = ["Positivo", "Negativo", "Neutro"]
    metricas = metrics.classification_report(classes, resultados, sentimentos)
    print("As métricas obtidas para o modelo são")
    print(metricas)
    print('\n')
    
    sentimentos = ["Negativo", "Neutro", "Positivo"]
    matrizConfusao2 = pd.crosstab(classes, resultados, rownames=["Real"], colnames=["Predito"], margins=True)
    matrizConfusao = confusion_matrix(classes, resultados)
    matrizConfusao = pd.DataFrame(matrizConfusao)
    matrizConfusao.index = sentimentos
    matrizConfusao.columns = sentimentos
    print("Matrz de Confusão para o modelo\n")
    print(matrizConfusao2)
    
    #Apenas testar para a aplicação de análise isolada, fora do mymobiconf
    #sn.set(font_scale=1.6)
    #figura_matriz = sn.heatmap(matrizConfusao, annot=True,linewidths=2, vmax=3000, vmin=0, square=True)# font size
    #plt.show()
    
    
