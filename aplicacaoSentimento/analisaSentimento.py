from . import limpezaDeDados
from . import conexaoBD
from . import calculoScore
from decimal import Decimal
#from sklearn.externals.joblib import load Depreciada
from joblib import load
from . import constroiModelo

#frase = 'Estamos �� ♥ ���⛪ ☺�� 😁💓😤🤣 malçÇ123 Ççmal123 malÇ123çmal 23 bem34 @({marcos} nota 10 @açÇai #cupuaçu @ # 134lasanha goste@3i VAi bastante muit2243o stackoverflow.com/questions/15710793/python-remove-urls-from-text-with-regex Aqui @3223marcus21 estamos #_!123indo embora'

def executar_analise():
    #Carregando os modelos de treinamento criados e salvos em disco
    #Se estes nçao existirem, então é gerada uma exceção, executando o código de except. Este invoca o método para construir os modelos
    #O método os grava em disco e então eles podem ser carregados
    try:
        modeloSentimento = load('aplicacaoSentimento/modelo_sentimento.joblib')
        vetorProbabilidade = load('aplicacaoSentimento/vetor_probabilidade.joblib')
        frequencia_frases = load('aplicacaoSentimento/frequencia_frases.joblib')
    except:
        constroiModelo.gerar_gravar_modelo()
        modeloSentimento = load('aplicacaoSentimento/modelo_sentimento.joblib')
        vetorProbabilidade = load('aplicacaoSentimento/vetor_probabilidade.joblib')
        frequencia_frases = load('aplicacaoSentimento/frequencia_frases.joblib')

    opnioesAvaliar = []
    idOpniao = []
    conexao = conexaoBD.operacoesBD()
    listaOpnioes = conexao.selecionar_todas__opnioes()
    considerar = True
    for opniao in listaOpnioes:
        considerar = limpezaDeDados.verificar_ruido(opniao['opiniao'])
        if(opniao['probabilidade_positivo'] is None and considerar):
            opnioesAvaliar.append(limpezaDeDados.limpar_frase(opniao['opiniao']))
            idOpniao.append(opniao['ID'])
            #print(opniao['ID'])

    if(len(opnioesAvaliar) > 0):
        frequencia_testes = vetorProbabilidade.transform(opnioesAvaliar)
        resultado = modeloSentimento.predict_proba(frequencia_testes) #[Negativo, Neutro, Positivo]
        sentimentos = modeloSentimento.predict(frequencia_testes) #[Negativo, Neutro, Positivo]

        x = 0
        for classificacao in resultado:
            #print(idOpniao[x], round(Decimal(classificacao[2]),5), round(Decimal(classificacao[1]),5), round(Decimal(classificacao[0]),5))
            conexao.inserir_probabilidade(idOpniao[x], round(Decimal(classificacao[2]),5), round(Decimal(classificacao[1]),5), round(Decimal(classificacao[0]),5), sentimentos[x])
            x = x + 1

        listaProbabilidadesEventos = conexao.selecionar_probs_evento()
        calculoScore.calcular_score_eventos(conexao, listaProbabilidadesEventos)

        # Verifica se há eventos com score nulo
        listaProbabilidadesEventos = conexao.selecionar_eventos_prob_nula()
        if (len(listaProbabilidadesEventos) > 0):
            calculoScore.calcular_score_eventos(conexao, listaProbabilidadesEventos)

executar_analise()

#Código para leitura de todas as opiniões da base de dados, solicitação da rotulação de cada uma e escrita em arquivo txt
'''
opnioesEvento = conexaoBD.operacoesBD()
listaOpnioes = opnioesEvento.selecionar_todas__opnioes()
arquivoOpnioes = open('arquivoOpnioesEvento.txt', 'at')
conta = 0
for opniao in listaOpnioes:
    if(opniao['ID'] > 1106):
        print(str(opniao['opiniao']))
        classe = input('Informe a classe da frase: ')

        arquivoOpnioes.write(str(opniao['ID']) + '---' + str(opniao['opiniao'] + '---' + str(classe) + '\n'))

        if (conta == 100):
            break
        conta = conta + 1
arquivoOpnioes.close()
'''
