USE scape;

ALTER TABLE evento
ADD score_evento DECIMAL(6, 5);

ALTER TABLE opiniao
ADD probabilidade_positivo DECIMAL(6, 5);

ALTER TABLE opiniao
ADD probabilidade_negativo DECIMAL(6, 5);

ALTER TABLE opiniao
ADD probabilidade_neutro DECIMAL(6, 5);

ALTER TABLE opiniao
ADD sentimento VARCHAR(10);

