import string
import re
import  nltk
#nltk.download()
stop_words_coletadas = []
with open("aplicacaoSentimento/stop_words_git_560.txt","r") as f:
    for frase in f.read().split('\n'):
        stop_words_coletadas.append(frase)

pontuacao_digitos = []
for simbolo in string.punctuation:
    pontuacao_digitos.append(simbolo)

expressaoLink = re.compile(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))'''
, re.DOTALL)

expressaoSimbolo = re.compile(r'(@[A-Za-z0-9çÇ]+)|(@)|([A-Za-zçÇ]+[0-9]+[A-Za-zçÇ]+)|([A-Za-zçÇ]+[0-9]+)|([0-9]+[A-Za-zçÇ]+)', re.DOTALL)

def limpar_texto(frases):
    frasesLimpas = []
    for frase in frases:
        fraseLimpa = re.sub(expressaoLink, '', frase)
        #print(fraseLimpa)

        for simbolo in pontuacao_digitos:
            if (simbolo != '@' and simbolo != '#' and simbolo != '!'):
                fraseLimpa = fraseLimpa.replace(simbolo, '')
        for palavra in fraseLimpa.split():
            if palavra.lower() in stop_words_coletadas:
                fraseLimpa = fraseLimpa.replace(palavra, '')
        #print(fraseLimpa)

        fraseLimpa = re.sub(expressaoSimbolo, '', fraseLimpa)
        #print(fraseLimpa)

        frasesLimpas.append(str(fraseLimpa))


    return frasesLimpas

#Aplicação do método anterior para remoção de stop words de cada frase do texto. Em seguida é extraído o radical das palavras da frase, que também são associadas à emoção
def aplicarStemmer(frases):
    stemmer = nltk.stem.RSLPStemmer()  # Stemmer para o português
    frasesStemming = []
    palavrasComStemming = ''
    for frase in frases:
        for palavra in frase.split():
            palavrasComStemming = palavrasComStemming + str(stemmer.stem(palavra)) + ' '
        frasesStemming.append(palavrasComStemming)
        palavrasComStemming = ''
    return frasesStemming

def verificar_ruido(frase):
    stemmer = nltk.stem.RSLPStemmer()  # Stemmer para o português
    if(type(frase) != str):
        return False
    if (len(frase.split()) == 0):
        return False
    #if (len(frase.split()) == 1 and len(frase.split()[0]) <= 2):
        #return False
    for palavra in frase.split():
       verificacao = str(stemmer.stem(palavra))
       if (verificacao.lower() == 'test'):
            return False

    return True

def limpar_frase(frase):

    fraseLimpa = re.sub(expressaoLink, '', frase)
    #print(fraseLimpa)

    for simbolo in pontuacao_digitos:
        if (simbolo != '@' and simbolo != '#' and simbolo != '!'):
            fraseLimpa = fraseLimpa.replace(simbolo, '')
    for palavra in fraseLimpa.split():
        if palavra.lower() in stop_words_coletadas:
            fraseLimpa = fraseLimpa.replace(palavra, '')
    #print(fraseLimpa)

    fraseLimpa = re.sub(expressaoSimbolo, '', fraseLimpa)
    #print(fraseLimpa)

    return fraseLimpa
