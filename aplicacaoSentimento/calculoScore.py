from . import conexaoBD
from decimal import Decimal


def calcular_score_eventos(opnioesEvento, listaProbabilidadesEventos):
    somaScore = 0.0
    quantScores = 0
    idAtual = listaProbabilidadesEventos[0]['ID_EVENTO']
    listaScore = []
    for probEvento in listaProbabilidadesEventos:
        if(idAtual == probEvento['ID_EVENTO']):
            if((probEvento['probabilidade_positivo'] > probEvento['probabilidade_negativo']) and (probEvento['probabilidade_positivo'] > probEvento['probabilidade_neutro'])):
                somaScore = somaScore + float(probEvento['probabilidade_positivo'])
                quantScores = quantScores + 1
            elif((probEvento['probabilidade_negativo'] > probEvento['probabilidade_positivo']) and (probEvento['probabilidade_negativo'] > probEvento['probabilidade_neutro'])):
                somaScore = somaScore - float(probEvento['probabilidade_negativo'])
                quantScores = quantScores + 1
            else:
                somaScore = somaScore + 1 - float(probEvento['probabilidade_neutro'])
                quantScores = quantScores + 1
        else:
            mediaScore = somaScore/quantScores
            listaScore.append({'ID': idAtual, 'score_evento': mediaScore})
            idAtual = probEvento['ID_EVENTO']
            somaScore = 0.0
            quantScores = 0
            if ((probEvento['probabilidade_positivo'] > probEvento['probabilidade_negativo']) and (probEvento['probabilidade_positivo'] > probEvento['probabilidade_neutro'])):
                somaScore = somaScore + float(probEvento['probabilidade_positivo'])
                quantScores = quantScores + 1
            elif ((probEvento['probabilidade_negativo'] > probEvento['probabilidade_positivo']) and (probEvento['probabilidade_negativo'] > probEvento['probabilidade_neutro'])):
                somaScore = somaScore - float(probEvento['probabilidade_negativo'])
                quantScores = quantScores + 1
            else:
                somaScore = somaScore + float(probEvento['probabilidade_positivo']) - float(probEvento['probabilidade_negativo'])
                quantScores = quantScores + 1

    mediaScore = somaScore/quantScores
    listaScore.append({'ID': idAtual, 'score_evento': mediaScore})
    #print(listaScore)
    #print(len(listaScore))

    for evento in listaScore:
        #print(evento['ID'], round(Decimal(evento['score_evento']), 5))
        opnioesEvento.inserir_score(evento['ID'], round(Decimal(evento['score_evento']), 5))